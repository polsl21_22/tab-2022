import React from "react";
import ReactDOM from "react-dom/client";
import "./app/main_style.css";
import App from "./app";

export const ChangeBackground = (color: string) => {
  document.body.style.background = color;
};

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);

root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);
