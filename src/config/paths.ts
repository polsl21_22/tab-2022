const paths = {
  /**Shared */
  login: '/login',
  dashboard: '/dashboard',
  default: '/',
  all: '*',

  /** Adminitrator and Driver*/
  reportBug: '/report',

  /** Adminitrator and Database*/
  monthlyPlan: '/monthly',
  driverPlan: '/driver',
  allRecords: '/all-record',
  addDriver: '/add-driver',
  addVehicle: '/add-vehicle',
  addRoute: '/add-route',
  addPassing: '/add-passing',
  tableDriver: '/driver-table',
  allDrivers: '/drivers',

  /**Only Driver*/
  driverSinglePlan: '/my-plan',
  reportTime: '/report-time',
  myRecords: '/my-record',

  /** Only Database */
  addAdministrator: '/add-administrator',
  readBugs: '/bugs',
  allAdmins: '/admins',
  pgAdmin: '/pg-admin',
}

export default paths