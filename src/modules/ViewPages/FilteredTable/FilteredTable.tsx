import { useState } from 'react'

import AdapterDateFns from '@mui/lab/AdapterDateFns'
import DatePicker from '@mui/lab/DatePicker'
import LocalizationProvider from '@mui/lab/LocalizationProvider'
import { Button, FormControl, TextField } from '@mui/material'
import { TextFieldProps } from '@mui/material'
import { DataGrid } from '@mui/x-data-grid'
import moment from 'moment'

import { useGetFilteredRoutesQuery } from 'modules'

import { CustomToolBar, Title, tableType } from 'shared'

import { t } from 'config'

import { columns } from './FilteredTable.columns'
import {
  Container,
  DataGridContainer,
  FilterTableContainer,
  Stack,
  TopSection,
} from './FilteredTable.style'

const FilteredTable = () => {
  function formatDate(inputDate: string | null) {
    return moment(inputDate).toISOString()
  }

  const [minStartDate, setMinStartDate] = useState<string | null>(null)
  const [maxStartDate, setMaxStartDate] = useState<string | null>(null)
  const [minEndDate, setMinEndDate] = useState<string | null>(null)
  const [maxEndDate, setMaxEndDate] = useState<string | null>(null)
  const [name, setName] = useState<string>('')
  const [lastName, setLastName] = useState<string>('')
  const [manufacturer, setManufacturer] = useState<string>('')
  const [queryString, setQueryString] = useState('')

  const { data, isLoading, refetch } = useGetFilteredRoutesQuery(
    queryString.length > 1 ? queryString : ''
  )

  const handleFilter = () => {
    const queryString = (
      '?' +
      (minStartDate!
        ? 'data_roczpoczecia_min=' + formatDate(minStartDate) + '&'
        : '') +
      (maxStartDate!
        ? 'data_roczpoczecia_max=' + formatDate(maxStartDate) + '&'
        : '') +
      (minEndDate!
        ? 'data_zakonczenia_min=' + formatDate(minEndDate) + '&'
        : '') +
      (maxEndDate!
        ? 'data_zakonczenia_max=' + formatDate(maxEndDate) + '&'
        : '') +
      (name! ? 'imie_pracownika=' + name + '&' : '') +
      (lastName! ? 'nazwisko_pracownika=' + lastName + '&' : '') +
      (manufacturer! ? 'marka=' + manufacturer + '&' : '')
    ).slice(0, -1)
    setQueryString(queryString)
    refetch()
  }

  return (
    <>
      {!isLoading && (
        <TopSection>
          <Title title={t.pageLabels.filteredTable} />
          <FormControl></FormControl>
        </TopSection>
      )}
      <Container>
        <FilterTableContainer>
          <LocalizationProvider dateAdapter={AdapterDateFns}>
            <DatePicker
              views={['year', 'month', 'day']}
              value={minStartDate}
              label={t.forms.minStartDate}
              inputFormat="dd/MM/yyyy"
              onChange={(newValue: string | null) => {
                setMinStartDate(newValue)
              }}
              renderInput={(params: TextFieldProps) => (
                <TextField size="small" {...params} />
              )}
            />
            <DatePicker
              views={['year', 'month', 'day']}
              value={maxStartDate}
              label={t.forms.maxStartDate}
              inputFormat="dd/MM/yyyy"
              onChange={(newValue: string | null) => {
                setMaxStartDate(newValue)
              }}
              renderInput={(params: TextFieldProps) => (
                <TextField size="small" {...params} />
              )}
            />
            <DatePicker
              views={['year', 'month', 'day']}
              value={minEndDate}
              label={t.forms.minEndDate}
              inputFormat="dd/MM/yyyy"
              onChange={(newValue: string | null) => {
                setMinEndDate(newValue)
              }}
              renderInput={(params: TextFieldProps) => (
                <TextField size="small" {...params} />
              )}
            />
            <DatePicker
              views={['year', 'month', 'day']}
              value={maxEndDate}
              label={t.forms.maxEndDate}
              inputFormat="dd/MM/yyyy"
              onChange={(newValue: string | null) => {
                setMaxEndDate(newValue)
              }}
              renderInput={(params: TextFieldProps) => (
                <TextField size="small" {...params} />
              )}
            />
            <TextField
              label={t.forms.name}
              size="small"
              value={name}
              onChange={event => {
                setName(event.target.value)
              }}
              sx={{ width: '100%', maxWidth: '259px' }}
            />
            <TextField
              label={t.forms.lastName}
              size="small"
              value={lastName}
              onChange={event => {
                setLastName(event.target.value)
              }}
              sx={{ width: '100%', maxWidth: '259px' }}
            />
            <TextField
              label={t.forms.manufacturer}
              size="small"
              value={manufacturer}
              onChange={event => {
                setManufacturer(event.target.value)
              }}
              sx={{ width: '100%', maxWidth: '259px' }}
            />
            <Button
              sx={{
                height: '40px',
                width: '100%',
                maxWidth: '259px',
              }}
              onClick={handleFilter}
            >
              {t.buttons.filter}
            </Button>
          </LocalizationProvider>
        </FilterTableContainer>
        <DataGridContainer>
          <DataGrid
            sx={{ fontSize: '12px' }}
            rows={(!isLoading && data && data) || []}
            columns={columns}
            autoPageSize
            disableSelectionOnClick
            components={{
              Toolbar: () =>
                CustomToolBar('FilteredRoutes', data, tableType.filteredTable, ''),
              NoRowsOverlay: () => <Stack>{t.tablesErrors.noRoutes}</Stack>,
              NoResultsOverlay: () => <Stack>{t.tablesErrors.noRoutes}</Stack>,
            }}
          />
        </DataGridContainer>
      </Container>
    </>
  )
}
export default FilteredTable
