import { Stack as BaseStack, styled } from '@mui/material'

export const Container = styled('div')({
  margin: '2% 10%',
  height: '51vh',
  display: 'flex',
})
export const TopSection = styled('div')({
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
})

export const Stack = styled(BaseStack)({
  height: '100%',
  alignItems: 'center',
  justifyContent: 'center',
})

export const DataGridContainer = styled('div')({
  height: '100%',
  width: '70%',
  background: '#FFFFFF',
  fontSize: 'small',
})

export const FilterTableContainer = styled('div')({
  height: '100%',
  width: '25%',
  marginRight: '5%',
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  alignItems: 'center',
  background: '#FFFFFF',
  gap: '10px',
})
