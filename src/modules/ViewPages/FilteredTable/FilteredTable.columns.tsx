import { Tooltip } from '@mui/material';
import { GridColDef } from '@mui/x-data-grid';
import moment from 'moment';



import { t } from 'config';


export const columns: GridColDef[] = [
  { field: 'id', headerName: t.tables.id, width: 60, type: 'number' },
  {
    field: 'driver',
    headerName: t.tables.driver,
    flex: 2,
    renderCell: params => (
      <Tooltip
        title={
          params.row?.Kierowca ? (
            <>
              <p>{t.tables.name + ': ' + params.row?.Kierowca?.imie}</p>
              <p>{t.tables.lastName + ': ' + params.row?.Kierowca?.nazwisko}</p>
              <p>{t.tables.pesel + ': ' + params.row?.Kierowca?.pesel}</p>
              <p>{t.tables.login + ': ' + params.row?.Kierowca?.login}</p>
              <p>
                {t.tables.categories + ': '}
                {params.row?.Kierowca?.kategorie === null
                  ? t.tablesErrors.missing
                  : params.row?.Kierowca?.kategorie}
              </p>
            </>
          ) : (
            <p>{t.tablesErrors.errorDriverInfo}</p>
          )
        }
      >
        <span>
          {params.row?.Kierowca
            ? params.row?.Kierowca?.imie + ' ' + params.row?.Kierowca?.nazwisko
            : t.tables.null}
        </span>
      </Tooltip>
    ),
  },
  {
    field: 'vehicle',
    headerName: t.tables.vehicle,
    width: 200,
    flex: 2,
    renderCell: params => (
      <Tooltip
        title={
          <>
            <p>
              {t.tables.manufacturer + ': ' + params.row?.Pojazd.Marka.nazwa}
            </p>
            <p>{t.tables.model + ': ' + params.row?.Pojazd.model}</p>
            <p>
              {t.tables.registration +
                ': ' +
                params.row?.Pojazd.numer_rejestracyjny}
            </p>
            <p>{t.tables.vin + ': ' + params.row?.Pojazd.vin}</p>
            <p>
              {t.tables.capacity + ': ' + params.row?.Pojazd.pojemnosc_silnika}
            </p>
          </>
        }
      >
        <span>
          {params.row?.Pojazd.model +
            ' ' +
            params.row.Pojazd.numer_rejestracyjny}
        </span>
      </Tooltip>
    ),
  },
  {
    field: 'route',
    headerName: t.tables.route,
    valueGetter: params =>
      params.row.Trasa.miejscowosc_poczatkowa +
      ' → ' +
      params.row.Trasa.miejscowosc_koncowa,
    width: 250,
    flex: 2,
  },
  {
    field: 'passing date',
    headerName: t.tables.passingDate,
    flex: 2,
    renderCell: params => (
      <Tooltip
        title={
          <>
            <p>{t.tables.startDate + ': '}</p>
            <p>
              {moment(params.row.data_rozpoczecia).format('DD.MM.YYYY - HH:mm')}
            </p>
            <p>{t.tables.endDate + ': '}</p>
            <p>
              {moment(params.row.data_zakonczenia).format('DD.MM.YYYY - HH:mm')}
            </p>

            {params.row.czas_przejazdu && (
              <>
                <p>{t.tables.routeTime + ':'}</p>
                <p>{params.row.czas_przejazdu}</p>
              </>
            )}
          </>
        }
      >
        <span>
          {moment(params.row.data_rozpoczecia).format('DD.MM.YYYY') +
            ' - ' +
            moment(params.row.data_zakonczenia).format('DD.MM.YYYY')}
        </span>
      </Tooltip>
    ),
  },
  {
    field: 'load',
    headerName: t.tables.load,
    type: 'number',
    valueGetter: params => params.row.ladunek,
    width: 110,
  },
]