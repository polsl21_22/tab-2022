import { useState } from 'react'

import AdapterDateFns from '@mui/lab/AdapterDateFns'
import DatePicker from '@mui/lab/DatePicker'
import LocalizationProvider from '@mui/lab/LocalizationProvider'
import { FormControl } from '@mui/material'
import { TextFieldProps } from '@mui/material'
import { DataGrid } from '@mui/x-data-grid'
import moment from 'moment'

import { useGetFilteredRoutesQuery } from 'modules'

import { CustomToolBar, Title, tableType } from 'shared'

import { t } from 'config'

import { columns } from './MonthlyTable.columns'
import {
  Container,
  DataGridContainer,
  DatePickerTextField,
  Stack,
  TopSection,
} from './MonthlyTable.style'

const MonthlyTable = () => {
  const [date, setDate] = useState<string | null>(null)
  const [queryString, setQueryString] = useState('')

  const { data, isLoading, refetch } = useGetFilteredRoutesQuery(
    queryString.length > 1 ? queryString : ''
  )

  const handleFilter = () => {
    const queryString = (
      '?' +
      (date!
        ? 'data_zakonczenia_min=' +
          moment(date).startOf('month').toISOString() +
          '&'
        : '') +
      (date!
        ? 'data_roczpoczecia_max=' +
          moment(date).endOf('month').toISOString() +
          '&'
        : '')
    ).slice(0, -1)
    setQueryString(queryString)
    refetch()
  }

  return (
    <>
      {!isLoading && (
        <TopSection>
          <Title title={t.pageLabels.monthlyTable} />
          <FormControl>
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <DatePicker
                views={['year', 'month']}
                value={date}
                inputFormat="MM/yyyy"
                onChange={(newValue: string | null) => {
                  setDate(newValue)
                }}
                onMonthChange={() => {
                  handleFilter()
                }}
                renderInput={(params: TextFieldProps) => (
                  <DatePickerTextField
                    size="small"
                    {...params}
                    inputProps={{
                      ...params.inputProps,
                      placeholder: t.forms.monthSelect,
                    }}
                  />
                )}
              />
            </LocalizationProvider>
          </FormControl>
        </TopSection>
      )}
      <Container>
        <DataGridContainer>
          <DataGrid
            sx={{ fontSize: '12px' }}
            rows={(!isLoading && data && data) || []}
            columns={columns}
            disableSelectionOnClick
            components={{
              Toolbar: () =>
                CustomToolBar(
                  'MonthlyTable-' + moment().format('MM-YYYY'),
                  data,
                  tableType.monthlyTable,
                  ''
                ),
              NoRowsOverlay: () => <Stack>{t.tablesErrors.noRoutes}</Stack>,
              NoResultsOverlay: () => <Stack>{t.tablesErrors.noRoutes}</Stack>,
            }}
          />
        </DataGridContainer>
      </Container>
    </>
  )
}
export default MonthlyTable
