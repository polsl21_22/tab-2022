import {
  Stack as BaseStack,
  TextField as BaseTextField,
  styled,
} from '@mui/material'

export const Container = styled('div')({
  margin: '2% 10%',
  height: '51vh',
  display: 'flex',
})
export const TopSection = styled('div')({
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
})

export const Stack = styled(BaseStack)({
  height: '100%',
  alignItems: 'center',
  justifyContent: 'center',
})

export const DataGridContainer = styled('div')({
  height: '100%',
  width: '100%',
  background: '#FFFFFF',
  fontSize: 'small',
})

export const DatePickerTextField = styled(BaseTextField)({
  margin: '1% 2%',
  width: '360px',
  borderRadius: '5px',
  backgroundColor: '#C4C4C4',
})
