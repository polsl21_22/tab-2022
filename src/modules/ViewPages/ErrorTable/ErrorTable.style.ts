import { styled } from '@mui/material'


export const Container = styled('div')({
  margin: '2% 10%',
  height: '51vh',
  backgroundColor: '#FFFFFF',
  overflow: 'scroll',
})

