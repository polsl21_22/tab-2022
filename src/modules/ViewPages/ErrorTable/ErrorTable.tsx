import { Fragment, useEffect, useState } from 'react'
import { useForm } from 'react-hook-form'

import DeleteOutlineIcon from '@mui/icons-material/DeleteOutline'
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown'
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp'
import {
  Box,
  Button,
  Collapse,
  IconButton,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from '@mui/material'
import moment from 'moment'

import { useDeleteReportByIdMutation, useGetReportQuery } from 'modules'

import { ErrorReport as ReportType } from 'shared'

import { t } from 'config'

import { Container } from './ErrorTable.style'

const ErrorTable = () => {
  const [reports, setReports] = useState<ReportType[] | null>(null)

  const { data, isLoading } = useGetReportQuery('')

  useEffect(() => {
    setReports(data)
  }, [data])

  const Row = (props: { report: ReportType }) => {
    const { report } = props
    const methods = useForm<ReportType>({ defaultValues: report })
    const [open, setOpen] = useState(false)
    const [deleteReport] = useDeleteReportByIdMutation()
    const { handleSubmit } = methods
    const onSubmit = async (reportToDelete: ReportType) => {
      await deleteReport(reportToDelete.id)
    }
    return (
      <Fragment>
        <TableRow sx={{ '& > *': { borderBottom: 'unset' } }}>
          <TableCell>
            <IconButton
              aria-label="expand row"
              size="small"
              onClick={() => setOpen(!open)}
            >
              {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
            </IconButton>
          </TableCell>
          <TableCell component="th" scope="row">
            {report.id}
          </TableCell>
          <TableCell align="right">{report.tytul}</TableCell>
          <TableCell align="right">{report.kategoria}</TableCell>
          <TableCell align="right">
            {moment(report.data_utworzenia).format('DD.MM.YYYY, HH:mm')}
          </TableCell>
          <TableCell align="right">
            <Button onClick={handleSubmit(onSubmit)} color="error">
              <DeleteOutlineIcon />
            </Button>
          </TableCell>
        </TableRow>
        <TableRow>
          <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
            <Collapse in={open} timeout="auto" unmountOnExit>
              <Box sx={{ margin: 1 }}>
                <Typography variant="h5" gutterBottom component="div">
                  {t.tables.description}
                </Typography>
                <Typography variant="subtitle1" gutterBottom component="div">
                  {report.opis}
                </Typography>
              </Box>
            </Collapse>
          </TableCell>
        </TableRow>
      </Fragment>
    )
  }

  return (
    <>
      <Container>
        <TableContainer component={Paper}>
          {!isLoading && data && (
            <Table stickyHeader aria-label="sticky table">
              <TableHead>
                <TableRow>
                  <TableCell />
                  <TableCell>{t.tables.id}</TableCell>
                  <TableCell align="right">{t.tables.title}</TableCell>
                  <TableCell align="right">{t.tables.category}</TableCell>
                  <TableCell align="right">{t.tables.data}</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {reports?.map(report => (
                  <Row key={report.id} report={report} />
                ))}
              </TableBody>
            </Table>
          )}
        </TableContainer>
      </Container>
    </>
  )
}

export default ErrorTable
