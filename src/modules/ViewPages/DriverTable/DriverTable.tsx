import { useEffect, useState } from 'react'

import { Autocomplete, FormControl, TextField } from '@mui/material'
import { DataGrid } from '@mui/x-data-grid'

import { useGetDriversQuery, useGetRoutesByIdQuery } from 'modules'

import { CustomToolBar, Title, tableType } from 'shared'
import { driver as Driver } from 'shared'

import { t } from 'config'

import { columns } from './DriverTable.columns'
import {
  Container,
  DataGridContainer,
  Stack,
  TopSection,
} from './DriverTable.style'

const DriverTable = () => {
  const [driver, setDriver] = useState<Driver>()
  const [inputValue, setInputValue] = useState('')
  const { data, isLoading } = useGetDriversQuery('')
  const {
    data: driverRoutesData,
    refetch,
    isLoading: isLoadingDriverRoutes,
  } = useGetRoutesByIdQuery(
    driver ? (driver.kierowca_id ? driver.kierowca_id : 0) : 0
  )
  useEffect(() => {
    if (driver) refetch()
  }, [driver, refetch])

  return (
    <>
      {!isLoading && (
        <TopSection>
          <Title title={t.pageLabels.driverTable} />
          <FormControl>
            <Autocomplete
              sx={{ background: '#FFFFFF', width: '250px' }}
              value={driver || null}
              onChange={(_, newValue) => {
                newValue && setDriver(newValue)
              }}
              inputValue={inputValue}
              options={data || []}
              onInputChange={(_, newInputValue) => {
                setInputValue(newInputValue)
              }}
              isOptionEqualToValue={(option, value) => option.id === value.id}
              getOptionLabel={(record: Driver) =>
                record.kierowca_id + ' ' + record.imie + ' ' + record.nazwisko
              }
              renderInput={params => <TextField {...params} />}
            />
          </FormControl>
        </TopSection>
      )}
      <Container>
        <DataGridContainer>
          <DataGrid
            rows={
              (!isLoadingDriverRoutes &&
                driverRoutesData &&
                driverRoutesData) ||
              []
            }
            columns={columns}
            autoPageSize
            disableSelectionOnClick
            components={{
              Toolbar: () =>
                CustomToolBar(
                  'DriverTable',
                  driverRoutesData,
                  tableType.driverTable,
                  driver?.imie +
                    ' ' +
                    driver?.nazwisko +
                    ' (' +
                    driver?.login +
                    ')'
                ),
              NoRowsOverlay: () => (
                <Stack>{t.tablesErrors.noRoutesDriver}</Stack>
              ),
              NoResultsOverlay: () => <Stack>{t.tablesErrors.noRoutes}</Stack>,
            }}
          />
        </DataGridContainer>
      </Container>
    </>
  )
}
export default DriverTable
