import { useEffect, useState } from 'react'

import { DataGrid } from '@mui/x-data-grid'

import { useGetAdministratorQuery } from 'modules'

import { CustomToolBar, Title, administrator, tableType } from 'shared'

import { t } from 'config'

import { columns } from './AdministratorTable.columns'
import {
  Container,
  DataGridContainer,
  Stack,
  TopSection,
} from './AdministratorTable.style'

const AdministratorTable = () => {
  const [admins, setAdmins] = useState<administrator[] | null>(null)
  const { data, isLoading } = useGetAdministratorQuery('')

  useEffect(() => {
    setAdmins(data)
  }, [data])

  return (
    <>
      {!isLoading && (
        <TopSection>
          <Title title={t.pageLabels.driversTable} />
        </TopSection>
      )}
      <Container>
        <DataGridContainer>
          <DataGrid
            rows={admins || []}
            columns={columns}
            disableSelectionOnClick
            components={{
              Toolbar: () =>
                CustomToolBar(
                  'AdministratorTable',
                  data,
                  tableType.adminsTable,
                  ''
                ),
              NoRowsOverlay: () => (
                <Stack>{t.tablesErrors.noAdministrator}</Stack>
              ),
              NoResultsOverlay: () => (
                <Stack>{t.tablesErrors.noAdministrator}</Stack>
              ),
            }}
          />
        </DataGridContainer>
      </Container>
    </>
  )
}
export default AdministratorTable
