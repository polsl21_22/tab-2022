import { GridColDef } from '@mui/x-data-grid'

import { t } from 'config'

export const columns: GridColDef[] = [
  {
    field: 'administracja_id',
    headerName: t.tables.id,
    width: 60,
    type: 'number',
  },
  {
    field: 'imie',
    headerName: t.tables.name,
    width: 200,
    flex: 1,
  },
  {
    field: 'nazwisko',
    headerName: t.tables.lastName,
    width: 250,
    flex: 1,
  },
  {
    field: 'pesel',
    headerName: t.tables.pesel,
    width: 250,
    flex: 1,
  },
  {
    field: 'login',
    headerName: t.tables.login,
    width: 155,
    flex: 1,
  },
  {
    field: 'stanowisko_administracyjne_id',
    headerName: t.tables.administratorPosition,
    valueGetter: params =>
      params.row.StanowiskoAdministracyjne.typ,
    width: 155,
    flex: 1,
  },
]
