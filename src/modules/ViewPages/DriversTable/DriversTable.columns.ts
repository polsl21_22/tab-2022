import { GridColDef, GridRenderCellParams } from '@mui/x-data-grid'

import { t } from 'config'

const printCategories = (params: GridRenderCellParams) => {
  var kategorie = ''
  if (params.row.kategorie)
    if (params.row.kategorie.length > 0)
      params.row.kategorie.forEach((element: { kategoria: number }) => {
        kategorie += element.kategoria + ', '
      })
  if (kategorie.length > 2) kategorie.slice(0, -2)
  return kategorie
}

export const columns: GridColDef[] = [
  { field: 'kierowca_id', headerName: t.tables.id, width: 60, type: 'number' },
  {
    field: 'imie',
    headerName: t.tables.name,
    width: 200,
    flex: 1,
  },
  {
    field: 'nazwisko',
    headerName: t.tables.lastName,
    width: 250,
    flex: 1,
  },
  {
    field: 'pesel',
    headerName: t.tables.pesel,
    width: 250,
    flex: 1,
  },
  {
    field: 'login',
    headerName: t.tables.login,
    width: 155,
    flex: 1,
  },
  {
    field: 'kategorie',
    headerName: t.tables.categories,
    renderCell: params => printCategories(params),
    width: 155,
    flex: 1,
  },
]
