import { Stack as BaseStack, styled } from '@mui/material'

export const Container = styled('div')({
  margin: '2% 10%',
  height: '51vh',
  display: 'grid',
  placeItems: 'center',
  backgroundColor: '#FFFFFF',
})
export const TopSection = styled('div')({
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
})

export const Stack = styled(BaseStack)({
  height: '100%',
  alignItems: 'center',
  justifyContent: 'center',
})

export const DataGridContainer = styled('div')({
  height: '100%',
  width: '100%',
  background: '#FFFFFF',
})
