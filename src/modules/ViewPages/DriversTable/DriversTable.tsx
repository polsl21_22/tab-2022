import { useEffect, useState } from 'react'
import { useNavigate } from 'react-router-dom'

import { FormControl, TextField } from '@mui/material'
import { DataGrid } from '@mui/x-data-grid'

import { useGetDriversQuery } from 'modules'

import { CustomToolBar, Title, categories, tableType } from 'shared'
import { driver as Driver } from 'shared'

import { paths, t } from 'config'

import { columns } from './DriversTable.columns'
import {
  Container,
  DataGridContainer,
  Stack,
  TopSection,
} from './DriversTable.style'

const DriversTable = () => {
  const [drivers, setDrivers] = useState<Driver[] | null>(null)
  const { data, isLoading } = useGetDriversQuery('')
  const navigate = useNavigate()

  useEffect(() => {
    setDrivers(data)
  }, [data])

  const catString = (categories: categories[] | null) => {
    var cat = ''
    if (categories)
      if (categories.length > 0)
        categories.forEach(c => (cat += c.kategoria + ', '))
    return categories ? cat.toLocaleLowerCase() : ' '
  }

  return (
    <>
      {!isLoading && (
        <TopSection>
          <Title title={t.pageLabels.driversTable} />
          <FormControl>
            <TextField
              placeholder={t.forms.search}
              sx={{ background: '#FFFFFF', width: '300px' }}
              onChange={event => {
                setDrivers(
                  data.filter((driver: Driver) => {
                    return event.target.value.toString().length > 0
                      ? event.target.value
                          .split(' ')
                          .filter(val => val)
                          .every(val => {
                            val = val.toLocaleLowerCase()
                            if (
                              (driver.imie.toLocaleLowerCase().includes(val) ||
                                driver.login
                                  .toLocaleLowerCase()
                                  .includes(val) ||
                                driver.nazwisko
                                  .toLocaleLowerCase()
                                  .includes(val) ||
                                driver.pesel.toString().includes(val) ||
                                catString(driver.kategorie).includes(val)) &&
                              val.length > 0
                            )
                              return true
                            return false
                          })
                        ? driver
                        : null
                      : data
                  })
                )
              }}
            />
          </FormControl>
        </TopSection>
      )}
      <Container>
        <DataGridContainer>
          <DataGrid
            rows={drivers || []}
            columns={columns}
            disableSelectionOnClick
            onRowDoubleClick={props => {
              navigate(paths.driverPlan, { state: props.row })
            }}
            components={{
              Toolbar: () =>
                CustomToolBar('DriversTable', data, tableType.driversTable, ''),
              NoRowsOverlay: () => <Stack>{t.tablesErrors.noDriver}</Stack>,
              NoResultsOverlay: () => <Stack>{t.tablesErrors.noDriver}</Stack>,
            }}
          />
        </DataGridContainer>
      </Container>
    </>
  )
}
export default DriversTable
