import { Tooltip } from '@mui/material';
import { GridColDef } from '@mui/x-data-grid';
import moment from 'moment';



import { t } from 'config';


export const columns: GridColDef[] = [
  { field: 'id', headerName: 'ID', width: 60, type: 'number' },
  {
    field: 'vehicle',
    headerName: 'Vehicle',
    renderCell: params => (
      <Tooltip
        title={
          <>
            <p>
              {t.tables.manufacturer + ': ' + params.row?.Pojazd.Marka.nazwa}
            </p>
            <p>{t.tables.model + ': ' + params.row?.Pojazd.model}</p>
            <p>
              {t.tables.registration +
                ': ' +
                params.row?.Pojazd.numer_rejestracyjny}
            </p>
            <p>{t.tables.vin + ': ' + params.row?.Pojazd.vin}</p>
            <p>
              {t.tables.capacity + ': ' + params.row?.Pojazd.pojemnosc_silnika}
            </p>
          </>
        }
      >
        <span>
          {params.row?.Pojazd.model +
            ' ' +
            params.row.Pojazd.numer_rejestracyjny}
        </span>
      </Tooltip>
    ),
    width: 200,
    flex: 1,
  },
  {
    field: 'route',
    headerName: t.tables.route,
    valueGetter: params =>
      params.row.Trasa.miejscowosc_poczatkowa +
      ' → ' +
      params.row.Trasa.miejscowosc_koncowa,
    flex: 2,
  },
  {
    field: 'passing date',
    headerName: t.tables.passingDate,
    flex: 2,
    renderCell: params => (
      <Tooltip
        title={
          <>
            <p>{t.tables.startDate + ':'}</p>
            <p>
              {moment(params.row.data_rozpoczecia).format('DD.MM.YYYY - HH:mm')}
            </p>
            <p>{t.tables.endDate + ':'}</p>
            <p>
              {moment(params.row.data_zakonczenia).format('DD.MM.YYYY - HH:mm')}
            </p>
          </>
        }
      >
        <span>
          {moment(params.row.data_rozpoczecia).format('DD.MM.YYYY') +
            ' - ' +
            moment(params.row.data_zakonczenia).format('DD.MM.YYYY')}
        </span>
      </Tooltip>
    ),
  },
  {
    field: 'ladunek',
    headerName: t.tables.load,
    type: 'number',
    width: 110,
  },
  {
    field: 'reported',
    type: 'boolean',
    headerName: t.tables.reported,
    valueGetter: params => (params.row.czas_przejazdu ? true : false),
  },
]