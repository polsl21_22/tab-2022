import { useNavigate } from 'react-router-dom'

import { DataGrid } from '@mui/x-data-grid'

import { useGetMyRoutesQuery } from 'modules'

import { CustomToolBar, Title, tableType } from 'shared'

import { paths, t } from 'config'

import { columns } from './PreviousTable.columns'
import {
  Container,
  DataGridContainer,
  Stack,
  TopSection,
} from './PreviousTable.style'

const DriverTable = () => {
  const { data, isLoading } = useGetMyRoutesQuery('max')
  const navigate = useNavigate()
  return (
    <>
      {!isLoading && (
        <TopSection>
          <Title title={t.pageLabels.myRecords} />
        </TopSection>
      )}
      <Container>
        <DataGridContainer>
          <DataGrid
            rows={data || []}
            loading={isLoading}
            columns={columns}
            autoPageSize
            onRowDoubleClick={props => {
              navigate(paths.reportTime, { state: props.row })
            }}
            components={{
              Toolbar: () =>
                CustomToolBar('DriverTable', data, tableType.driverTable, ''),
              NoRowsOverlay: () => (
                <Stack>{t.tablesErrors.noRoutesDriver}</Stack>
              ),
              NoResultsOverlay: () => <Stack>{t.tablesErrors.noRoutes}</Stack>,
            }}
          />
        </DataGridContainer>
      </Container>
    </>
  )
}
export default DriverTable
