import { DataGrid } from '@mui/x-data-grid'

import { useGetMyRoutesQuery } from 'modules'

import { CustomToolBar, Title, tableType } from 'shared'

import { paths, t } from 'config'

import { columns } from './MyPlan.columns'
import { Container, DataGridContainer, Stack, TopSection } from './MyPlan.style'
import { useNavigate } from 'react-router-dom'

const MyPlan = () => {
  const { data, isLoading } = useGetMyRoutesQuery('min')
  const navigate = useNavigate()
  return (
    <>
      {!isLoading && (
        <TopSection>
          <Title title={t.pageLabels.myPlan} />
        </TopSection>
      )}
      <Container>
        <DataGridContainer>
          <DataGrid
            rows={data || []}
            loading={isLoading}
            columns={columns}
            autoPageSize
            disableSelectionOnClick
            onRowDoubleClick={props => {
              navigate(paths.reportTime, { state: props.row })
            }}
            components={{
              Toolbar: () =>
                CustomToolBar('MyPlan', data, tableType.driverTable, ''),
              NoRowsOverlay: () => (
                <Stack>{t.tablesErrors.noRoutesDriver}</Stack>
              ),
              NoResultsOverlay: () => <Stack>{t.tablesErrors.noRoutes}</Stack>,
            }}
          />
        </DataGridContainer>
      </Container>
    </>
  )
}
export default MyPlan
