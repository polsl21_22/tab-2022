import { styled } from '@mui/material'

export const Container = styled('div')({
  margin: '2% 10%',
  height: '70vh',
})
export const TopSection = styled('div')({
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
})
