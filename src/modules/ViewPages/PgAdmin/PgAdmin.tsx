import { Title } from 'shared'

import { host, t } from 'config'

import { Container, TopSection } from './PgAdmin.style'

const pgAdminSrc = host

const PgAdmin = () => {
  return (
    <>
      <TopSection>
        <Title title={t.pageLabels.pgAdmin} />
      </TopSection>
      <Container>
        <object
          title={t.pageLabels.pgAdminObj}
          id="inlinePgadmin"
          data={pgAdminSrc}
          width="100%"
          height="100%"
        >
          <embed src={pgAdminSrc} width="100%" height="100%"></embed>
        </object>
      </Container>
    </>
  )
}

export default PgAdmin
