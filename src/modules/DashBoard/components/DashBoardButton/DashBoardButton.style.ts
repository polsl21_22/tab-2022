import { Button as BaseButton, styled } from '@mui/material'

export const Button = styled(BaseButton)({
  backgroundColor: '#C4C4C4',
  margin: '2% 1%',
  height: '120px',
  width: '350px',
  fontSize: '100%',
  color: '#000000',
  '&:hover': {
    backgroundColor: '#F5EAEA',
  },
})
