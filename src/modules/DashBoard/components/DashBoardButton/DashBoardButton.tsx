import { useNavigate } from 'react-router-dom'

import { DashBoardButtonProps } from 'shared'

import { Button } from './DashBoardButton.style'

const DashBoardButton = (props: DashBoardButtonProps) => {
  const navigate = useNavigate()
  const handleClick = () => {
    navigate(props.path)
  }

  return (
    <Button onClick={handleClick}>
      <p>{props.label}</p>
    </Button>
  )
}

export default DashBoardButton
