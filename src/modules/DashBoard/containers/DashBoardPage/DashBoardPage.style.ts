import { styled } from '@mui/material'

export const ContentContainer = styled('div')({
  display: 'flex',
  marginLeft: '13%',
  marginRight: '13%',
  flexWrap: 'wrap',
  width: '100%',
  justifyContent: 'space-evenly',
})

export const Container = styled('div')({
  display: 'flex',
  justifyContent: 'center',
})

export const Temp = styled('div')({
  margin: '0 1%',
  height: '1px',
  width: '350px',
})
