import DashBoardButton from 'modules/DashBoard/components/DashBoardButton'

import {
  DashBoardButtonProps,
  LS,
  NavBar,
  databaseRoutes,
  driverRoutes,
  userType,
} from 'shared'
import { adminRoutes } from 'shared'

import { Container, ContentContainer, Temp } from './DashBoardPage.style'

const DashBoard = () => {
  const Content = () => {
    switch (LS.getUserType()) {
      case userType.Admin:
        return (
          <>
            {adminRoutes.map((route: DashBoardButtonProps, key: number) => (
              <DashBoardButton
                key={key}
                path={route.path}
                label={route.label}
              />
            ))}
          </>
        )
      case userType.Driver:
        return (
          <>
            {driverRoutes.map((route: DashBoardButtonProps, key: number) => (
              <DashBoardButton
                key={key}
                path={route.path}
                label={route.label}
              />
            ))}
          </>
        )
      case userType.Database:
        return (
          <>
            {databaseRoutes.map((route: DashBoardButtonProps, key: number) => (
              <DashBoardButton
                key={key}
                path={route.path}
                label={route.label}
              />
            ))}
          </>
        )
      default:
        return <></>
    }
  }

  return (
    <>
      <NavBar />
      <Container>
        <ContentContainer>
          <Content />
          <Temp />
          <Temp />
          <Temp />
          <Temp />
          <Temp />
          <Temp />
        </ContentContainer>
      </Container>
    </>
  )
}

export default DashBoard
