import { Middleware, isRejectedWithValue } from '@reduxjs/toolkit'

import { LS } from 'shared'

import { history } from './history'

export const authMiddleware: Middleware = () => next => action => {
  if (isRejectedWithValue(action))
    if (
      action.payload.originalStatus === 401 ||
      action.payload.originalStatus === 403
    ) {
      LS.Logout()
      history.push('/login')
    }

  return next(action)
}
