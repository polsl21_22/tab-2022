import { FC, useEffect } from 'react'
import { Navigate, Route, Routes } from 'react-router-dom'

import { ChangeBackground } from 'index'

import { Template } from 'modules'
import {
  NewAdministrator,
  NewDriver,
  NewErrorReport,
  NewPassing,
  NewRoute,
  NewVehicle,
} from 'modules/AddPages'
import { LoginPage } from 'modules/Auth/containers'
import { DashBoardPage } from 'modules/DashBoard'
import {
  AdminTable,
  DriverTable,
  DriversTable,
  ErrorTable,
  MonthlyTable,
  MyPlan,
  PgAdmin,
  PreviousTable,
  RoutesTable,
} from 'modules/ViewPages'

import { LS, PickBackground, TemplatePropType, userType } from 'shared'

import { paths } from 'config'
import { ReportTime } from 'modules/PatchRoute'

const PrivateRoute: FC<TemplatePropType> = ({ component: Component }) => {
  const isAuthenticated = LS.isLoggedin()
  if (isAuthenticated) return <Template component={Component} />
  return <Navigate to={paths.login} />
}

const Dashboard: FC<TemplatePropType> = ({ component: Component }) => {
  const isAuthenticated = LS.isLoggedin()
  if (isAuthenticated) return <Component />
  return <Navigate to={paths.login} />
}

const SharedRoute: FC<TemplatePropType> = ({ component: Component }) => {
  const isAuthenticated = LS.isLoggedin()
  if (isAuthenticated) return <Navigate to={paths.dashboard} />
  return <Component />
}

export const SharedRoutes = () => {
  useEffect(() => {
    ChangeBackground(PickBackground(LS.getUserType() as userType))
  }, [])

  const Temp = () => {
    return <></>
  }

  return (
    <Routes>
      <Route
        path={paths.login}
        element={<SharedRoute component={LoginPage} />}
      />
      <Route
        path={paths.dashboard}
        element={<Dashboard component={DashBoardPage} />}
      />

      <Route
        path={paths.addDriver}
        element={<PrivateRoute component={NewDriver} />}
      />
      <Route
        path={paths.addAdministrator}
        element={<PrivateRoute component={NewAdministrator} />}
      />

      <Route
        path={paths.addVehicle}
        element={<PrivateRoute component={NewVehicle} />}
      />
      <Route
        path={paths.addRoute}
        element={<PrivateRoute component={NewRoute} />}
      />

      <Route
        path={paths.addPassing}
        element={<PrivateRoute component={NewPassing} />}
      />
      <Route
        path={paths.driverPlan}
        element={<PrivateRoute component={DriverTable} />}
      />
      <Route
        path={paths.allDrivers}
        element={<PrivateRoute component={DriversTable} />}
      />

      <Route
        path={paths.allAdmins}
        element={<PrivateRoute component={AdminTable} />}
      />
      <Route
        path={paths.pgAdmin}
        element={<PrivateRoute component={PgAdmin} />}
      />
      <Route
        path={paths.readBugs}
        element={<PrivateRoute component={ErrorTable} />}
      />

      {/**TODO: v Pages for those routes v */}

      <Route
        path={paths.reportBug}
        element={<PrivateRoute component={NewErrorReport} />}
      />

      <Route
        path={paths.monthlyPlan}
        element={<PrivateRoute component={MonthlyTable} />}
      />
      <Route
        path={paths.driverPlan}
        element={<PrivateRoute component={Temp} />}
      />
      <Route
        path={paths.allRecords}
        element={<PrivateRoute component={RoutesTable} />}
      />
      <Route
        path={paths.driverSinglePlan}
        element={<PrivateRoute component={MyPlan} />}
      />
      <Route
        path={paths.reportTime}
        element={<PrivateRoute component={ReportTime} />}
      />
      <Route
        path={paths.myRecords}
        element={<PrivateRoute component={PreviousTable} />}
      />
      <Route
        path={paths.readBugs}
        element={<PrivateRoute component={Temp} />}
      />
      <Route
        path={paths.default}
        element={<Navigate to={paths.login} replace />}
      />
      <Route path={paths.all} element={<Navigate to={paths.login} replace />} />
    </Routes>
  )
}
