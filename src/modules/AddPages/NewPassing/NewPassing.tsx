import { useEffect } from 'react'
import { useForm } from 'react-hook-form'

import { LoadingButton } from '@mui/lab'
import { Button, Paper, Typography } from '@mui/material'
import Swal from 'sweetalert2'

import { useAddPassingMutation } from 'modules/services/users.api'

import { passingPayload } from 'shared'

import { t } from 'config'

import { FormInputText } from '../FormComponents/FormInputText'

const defaultValues = {
  miejscowosc_poczatkowa: '',
  miejscowosc_koncowa: '',
}
const NewPassing = () => {
  const methods = useForm<passingPayload>({ defaultValues: defaultValues })
  const { handleSubmit, reset, control } = methods
  const [addPassing, { isError, isSuccess, isLoading }] =
    useAddPassingMutation()
  const onSubmit = (data: passingPayload) => {
    addPassing(data)
  }
  useEffect(() => {
    if (isError) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!',
      })
    }
    if (isSuccess) {
      Swal.fire('Good job!', 'Added record!', 'success')
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLoading])
  return (
    <Paper
      style={{
        display: 'grid',
        gridRowGap: '20px',
        padding: '20px',
        margin: '10px 300px',
      }}
    >
      <Typography variant="h4"> {t.pageLabels.newPassing}</Typography>

      <FormInputText
        name="miejscowosc_poczatkowa"
        control={control}
        label={t.forms.startingLocation}
        errorMsg={t.formsErrors.startingLocation}
      />
      <FormInputText
        name="miejscowosc_koncowa"
        control={control}
        label={t.forms.destinedLocation}
        errorMsg={t.formsErrors.destinedLocation}
      />

      <LoadingButton
        loading={isLoading}
        onClick={handleSubmit(onSubmit)}
        variant={'contained'}
      >
        {t.buttons.submit}
      </LoadingButton>
      <Button onClick={() => reset()} variant={'outlined'}>
        {t.buttons.reset}
      </Button>
    </Paper>
  )
}

export default NewPassing
