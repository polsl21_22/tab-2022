import { useEffect } from 'react'
import { useForm } from 'react-hook-form'

import { LoadingButton } from '@mui/lab'
import { Button, Paper, Typography } from '@mui/material'
import Swal from 'sweetalert2'

import {
  useAddVehicleMutation,
  useGetManufactureQuery,
} from 'modules/services/users.api'

import { manufacturePayload, vehiclePayload } from 'shared'

import { t } from 'config'

import { FormInputDropdown } from '../FormComponents/FormInputDropdown'
import { FormInputText } from '../FormComponents/FormInputText'

const defaultValues = {
  vin: '',
  pojemnosc_silnika: 0,
  marka_id: -1,
  numer_rejestracyjny: '',
  model: '',
}

const NewVehicle = () => {
  const methods = useForm<vehiclePayload>({ defaultValues: defaultValues })
  const { handleSubmit, reset, control } = methods
  const [addVehicle, { isLoading, isError, isSuccess }] =
    useAddVehicleMutation()
  const manufactureGet = useGetManufactureQuery('')

  const onSubmit = (data: vehiclePayload) => {
    data.pojemnosc_silnika = +data.pojemnosc_silnika
    addVehicle(data)
  }
  useEffect(() => {
    if (isError) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!',
      })
    }
    if (isSuccess) {
      Swal.fire('Good job!', 'Added record!', 'success')
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLoading])
  return (
    <Paper
      style={{
        display: 'grid',
        gridRowGap: '20px',
        padding: '20px',
        margin: '10px 300px',
      }}
    >
      <Typography variant="h4"> {t.pageLabels.newVehicle}</Typography>

      <FormInputText
        name="vin"
        control={control}
        label={t.forms.vin}
        errorMsg={t.formsErrors.vin}
      />
      <FormInputText
        name="pojemnosc_silnika"
        control={control}
        label={t.forms.capacity}
        errorMsg={t.formsErrors.capacity}
        regex={/^[1-9]\d*$/}
      />
      <FormInputText
        name="model"
        control={control}
        label={t.forms.model}
        errorMsg={t.formsErrors.model}
      />
      <FormInputText
        name="numer_rejestracyjny"
        control={control}
        label={t.forms.registration}
        errorMsg={t.formsErrors.registration}
      />
      {!manufactureGet.isLoading && (
        <FormInputDropdown
          name="marka_id"
          control={control}
          label={t.forms.manufacturer}
          errorMsg={t.formsErrors.manufacturer}
          options={
            manufactureGet.data
              ? manufactureGet.data.map((e: manufacturePayload) => ({
                  name: e.nazwa,
                  id: e.id,
                }))
              : []
          }
        />
      )}
      <LoadingButton
        loading={isLoading}
        onClick={handleSubmit(onSubmit)}
        variant={'contained'}
      >
        {t.buttons.submit}
      </LoadingButton>
      <Button onClick={() => reset()} variant={'outlined'}>
        {t.buttons.reset}
      </Button>
    </Paper>
  )
}

export default NewVehicle
