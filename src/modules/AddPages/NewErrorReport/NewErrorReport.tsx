import { useEffect, useState } from 'react'
import { Controller, useForm } from 'react-hook-form'

import { FormControl, MenuItem, Select, TextField } from '@mui/material'

import { useAddReportMutation } from 'modules/services/reports.api'

import { ErrorReport, Title, errorTypes } from 'shared'

import { t } from 'config'

import { Button, Content, MidSection, TopSection } from './NewErrorReport.style'

const defaultReport = {
  tytul: '',
  kategoria: '',
  opis: '',
}

const NewErrorReport = () => {
  const methods = useForm<ErrorReport>({ defaultValues: defaultReport })
  const { handleSubmit, control } = methods
  const [newReport, { data: resData, error: resError }] = useAddReportMutation()
  const [confirm, setConfirm] = useState({ value: '', success: true })
  const Submit = (data: ErrorReport) => {
    newReport(data)
  }
  useEffect(() => {
    if (resData) {
      setConfirm({ value: t.confirmation.reportAdd, success: true })
    } else if (resError) {
      setConfirm({ value: t.errors.reportAddError, success: false })
    }
  }, [resData, resError])

  const menuType = (err: { nazwa: string; id: number }[]) => {
    return errorTypes.map((type: { nazwa: string; id: number }) => {
      return (
        <MenuItem key={type.id} value={type.nazwa}>
          {type.nazwa}
        </MenuItem>
      )
    })
  }

  return (
    <>
      <TopSection>
        <Title title={t.pageLabels.reportBug} />
      </TopSection>
      <MidSection>
        <FormControl>
          <Controller
            render={({ field: { onChange, value } }) => (
              <Select
                onChange={onChange}
                value={value}
                sx={{ background: '#FFFFFF', width: '250px', color: '#000000' }}
              >
                {menuType(errorTypes)}
              </Select>
            )}
            control={control}
            name="kategoria"
            rules={{ required: true }}
          />
        </FormControl>
        <FormControl>
          <Controller
            name="tytul"
            control={control}
            render={({ field: { onChange } }) => (
              <TextField
                sx={{ background: '#FFFFFF' }}
                onChange={onChange}
                label={t.tables.title}
                variant="filled"
              />
            )}
          />
        </FormControl>
      </MidSection>
      <Content>
        <FormControl>
          <Controller
            name="opis"
            control={control}
            render={({ field: { onChange } }) => (
              <TextField
                id="outlined-multiline-static"
                multiline
                onChange={onChange}
                rows={10}
                placeholder={t.tables.details}
                sx={{ background: '#FFFFFF', width: '100%' }}
              />
            )}
          />
        </FormControl>
        <span
          style={{
            color: confirm.success ? 'green' : 'red',
            justifyContent: 'center',
            display: 'flex',
            fontSize: '20px',
          }}
        >
          {confirm.value}
        </span>
      </Content>
      <Button onClick={handleSubmit(Submit)}>{t.buttons.add}</Button>
    </>
  )
}

export default NewErrorReport
