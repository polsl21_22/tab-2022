import { Button as BaseButton, styled } from '@mui/material'

export const Content = styled('div')({
  margin: '2% 10%',
  height: '51vh',
  display: 'grid',
})
export const Controller = styled('div')({
  margin: '2% 10%',
  height: '51vh',
  display: 'grid',
})

export const TopSection = styled('div')({
  display: 'flex',
  flexDirection: 'row',
  alignItems: 'center',
})

export const MidSection = styled('div')({
  margin: '2% 10%',
  display: 'grid',
  gridTemplateColumns: 'auto 1fr',
  gap: '20px',
})

export const Button = styled(BaseButton)({
  position: 'absolute',
  bottom: '10%',
  height: '60px',
  width: '120px',
  right: '17%',
  backgroundColor: '#C4C4C4',
  fontSize: '100%',
  color: '#000000',
  '&:hover': {
    backgroundColor: '#F5EAEA',
  },
})
