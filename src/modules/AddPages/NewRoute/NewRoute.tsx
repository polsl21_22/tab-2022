import { useEffect } from 'react'
import { useForm } from 'react-hook-form'

import { LoadingButton } from '@mui/lab'
import { Button, Paper, Typography } from '@mui/material'
import Swal from 'sweetalert2'

import {
  useAddRouteMutation,
  useGetDriversQuery,
  useGetPassingsQuery,
  useGetVehiclesQuery,
} from 'modules/services/users.api'

import { routePayload } from 'shared'
import { driver as Driver } from 'shared'

import { t } from 'config'

import { FormInputDate } from '../FormComponents/FormInputDate'
import { FormInputDropdown } from '../FormComponents/FormInputDropdown'
import { FormInputText } from '../FormComponents/FormInputText'

const defaultValues = {
  data_rozpoczecia: new Date(),
  data_zakonczenia: new Date(),
  ladunek: 0,
  trasa_id: -1,
  kierowca_id: -1,
  pojazd_id: -1,
}
const NewRoute = () => {
  const methods = useForm<routePayload>({ defaultValues: defaultValues })
  const { handleSubmit, reset, control } = methods
  const [addRoute, { isError, isSuccess, isLoading }] = useAddRouteMutation()
  const onSubmit = (data: routePayload) => {
    data.ladunek = +data.ladunek
    data.pojazd_id = +data.pojazd_id!
    addRoute(data)
  }

  const passingsGet = useGetPassingsQuery('')
  const driversGet = useGetDriversQuery('')
  const vehiclesGet = useGetVehiclesQuery('')
  useEffect(() => {
    if (isError) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!',
      })
    }
    if (isSuccess) {
      Swal.fire('Good job!', 'Added record!', 'success')
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLoading])
  return (
    <Paper
      style={{
        display: 'grid',
        gridRowGap: '20px',
        padding: '20px',
        margin: '10px 300px',
      }}
    >
      <Typography variant="h4"> {t.pageLabels.newRoute}</Typography>
      <FormInputDate
        name="data_rozpoczecia"
        control={control}
        label={t.forms.startDate}
      />
      <FormInputDate
        name="data_zakonczenia"
        control={control}
        label={t.forms.endDate}
      />
      <FormInputText
        name="ladunek"
        control={control}
        label={t.forms.load}
        type="number"
        errorMsg={t.formsErrors.load}
        regex={
          /^(([0-5]?\d{0,4}|6[0-4]\d{3}|65[0-4]\d{2}|655[0-2]\d|6553[0-5])(,|(?=$)))+$/
        }
      />

      {!passingsGet.isLoading && (
        <FormInputDropdown
          name="trasa_id"
          control={control}
          options={
            passingsGet.data.map((e: any) => ({
              name: `${e.miejscowosc_poczatkowa} - ${e.miejscowosc_koncowa}`,
              id: e.id,
            })) || []
          }
          label={t.forms.passing}
          errorMsg={t.formsErrors.passing}
        />
      )}
      {!driversGet.isLoading && (
        <FormInputDropdown
          name="kierowca_id"
          control={control}
          options={
            driversGet.data
              ? driversGet.data.map((e: Driver) => ({
                  name: `${e.imie} ${e.nazwisko}`,
                  id: e.id,
                }))
              : []
          }
          label={t.forms.driver}
          errorMsg={t.formsErrors.driver}
        />
      )}
      {!vehiclesGet.isLoading && (
        <FormInputDropdown
          name="pojazd_id"
          control={control}
          options={
            vehiclesGet.data
              ? vehiclesGet.data.map((e: any) => ({
                  name: `${e.model} ${e.numer_rejestracyjny}`,
                  id: e.id,
                }))
              : []
          }
          label={t.forms.vehicle}
          errorMsg={t.formsErrors.vehicle}
        />
      )}

      <LoadingButton
        loading={isLoading}
        onClick={handleSubmit(onSubmit)}
        variant={'contained'}
      >
        {t.buttons.submit}
      </LoadingButton>
      <Button onClick={() => reset()} variant={'outlined'}>
        {t.buttons.reset}
      </Button>
    </Paper>
  )
}

export default NewRoute
