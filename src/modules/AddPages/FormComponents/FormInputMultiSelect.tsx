import { Controller } from 'react-hook-form'

import { FormControl, InputLabel, MenuItem, Select } from '@mui/material'
import Box from '@mui/material/Box'
import Chip from '@mui/material/Chip'
import OutlinedInput from '@mui/material/OutlinedInput'

import { FormInputProps } from '../../../shared/types/FormInputProps'

const ITEM_HEIGHT = 48
const ITEM_PADDING_TOP = 8
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
}

export const FormInputMultiSelect: React.FC<FormInputProps> = ({
  name,
  control,
  label,
  regex,
  options,
  getValues,
}) => {
  return (
    <FormControl size={'medium'}>
      <InputLabel>{label}</InputLabel>
      <Controller
        render={({ field: { onChange, value }, fieldState: { error } }) => (
          <Select
            multiple
            value={value}
            onChange={onChange}
            input={<OutlinedInput id="select-multiple-chip" label="Chip" />}
            renderValue={selected => (
              <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.5 }}>
                {selected.map((e: any) => (
                  <Chip key={e.id} label={e.name} />
                ))}
              </Box>
            )}
            MenuProps={MenuProps}
          >
            {options.map((opt: any) => (
              <MenuItem key={opt.id} value={opt}>
                {opt.name}
              </MenuItem>
            ))}
          </Select>
        )}
        control={control}
        name={name}
        rules={{ required: true }}
      />
    </FormControl>
  )
}
