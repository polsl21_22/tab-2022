import React from 'react'
import { Controller } from 'react-hook-form'

import { FormControl, InputLabel, MenuItem, Select } from '@mui/material'

import { FormInputProps } from 'shared/types/FormInputProps'

export const FormInputDropdown: React.FC<FormInputProps> = ({
  name,
  control,
  label,
  regex,
  options,
}) => {
  const generateSingleOptions = (options: { name: string; id: number }[]) => {
    return options.map((option: { name: string; id: number }) => {
      return (
        <MenuItem key={option.id} value={option.id}>
          {option.name}
        </MenuItem>
      )
    })
  }

  return (
    <FormControl size={'medium'}>
      <InputLabel>{label}</InputLabel>
      <Controller
        render={({ field: { onChange, value }, fieldState: { error } }) => (
          <Select label={label} onChange={onChange} value={value || null}>
            {generateSingleOptions(options)}
            <MenuItem key={-1} value={-1}>
              {''}
            </MenuItem>
          </Select>
        )}
        control={control}
        name={name}
        rules={{ required: true }}
      />
    </FormControl>
  )
}
