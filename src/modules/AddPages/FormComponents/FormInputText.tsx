import React from 'react'
import { Controller } from 'react-hook-form'

import { TextField } from '@mui/material'

import { FormInputProps } from '../../../shared/types/FormInputProps'

export const FormInputText = ({
  name,
  control,
  label,
  regex,
  errorMsg,
  type,
}: FormInputProps) => {
  return (
    <Controller
      name={name}
      control={control}
      rules={{ required: true, pattern: regex }}
      render={({
        field: { onChange, value },
        fieldState: { error },
        formState,
      }) => (
        <TextField
          helperText={error ? errorMsg : null}
          size="medium"
          error={!!error}
          type={type}
          onChange={onChange}
          value={value}
          fullWidth
          label={label}
          variant="outlined"
        />
      )}
    />
  )
}
