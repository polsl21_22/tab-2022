import { Controller } from 'react-hook-form'

import AdapterDateFns from '@mui/lab/AdapterDateFns'
import DatePicker from '@mui/lab/DatePicker'
import LocalizationProvider from '@mui/lab/LocalizationProvider'
import { TextField } from '@mui/material'

import { FormInputProps } from 'shared/types/FormInputProps'

export const FormInputDate = ({ name, control, label }: FormInputProps) => {
  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <Controller
        name={name}
        control={control}
        rules={{ required: true }}
        render={({ field: { onChange, value } }) => (
          <LocalizationProvider dateAdapter={AdapterDateFns}>
            <DatePicker
              label={label}
              value={value}
              onChange={onChange}
              renderInput={params => <TextField {...params} />}
            />
          </LocalizationProvider>
        )}
      />
    </LocalizationProvider>
  )
}
