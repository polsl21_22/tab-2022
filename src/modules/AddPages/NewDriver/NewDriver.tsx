import { useEffect } from 'react'
import { useForm } from 'react-hook-form'

import { LoadingButton } from '@mui/lab'
import { Button, Paper, Typography } from '@mui/material'
import Swal from 'sweetalert2'

import {
  useAddDriverMutation,
  useGetCategoriesQuery,
} from 'modules/services/users.api'

import { categories, driverPayload } from 'shared'

import { t } from 'config'

import { FormInputMultiSelect } from '../FormComponents/FormInputMultiSelect'
import { FormInputText } from '../FormComponents/FormInputText'

const defaultValues = {
  imie: '',
  nazwisko: '',
  pesel: '',
  kategorie: [],
  login: '',
  haslo: '',
}

const NewDriver = () => {
  const methods = useForm<driverPayload>({ defaultValues: defaultValues })
  const { handleSubmit, reset, control, getValues } = methods
  const [addDriver, { isLoading, isError, isSuccess }] = useAddDriverMutation()
  useEffect(() => {
    if (isError) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!',
      })
    }
    if (isSuccess) {
      Swal.fire('Good job!', 'Added record!', 'success')
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLoading])
  const categoriesGet = useGetCategoriesQuery('')
  const onSubmit = (data: driverPayload) => {
    data.kategorie = data.kategorie!.map((e: any) => e.id)
    addDriver(data)
  }

  return (
    <Paper
      style={{
        display: 'grid',
        gridRowGap: '20px',
        padding: '20px',
        margin: '10px 300px',
      }}
    >
      <Typography variant="h4">{t.pageLabels.newDriver}</Typography>
      {!categoriesGet.isLoading && (
        <FormInputMultiSelect
          getValues={getValues}
          control={control}
          options={
            categoriesGet.data
              ? categoriesGet.data.map((e: categories) => ({
                  name: e.kategoria,
                  id: e.id,
                }))
              : [{ name: 'No Data', id: -1 }]
          }
          name="kategorie"
          label={t.forms.category}
        />
      )}
      <FormInputText
        name="imie"
        control={control}
        label={t.forms.name}
        type="text"
        errorMsg={t.formsErrors.firstName}
      />
      <FormInputText
        name="nazwisko"
        control={control}
        label={t.forms.lastName}
        type="text"
        errorMsg={t.formsErrors.lastName}
      />
      <FormInputText
        name="pesel"
        control={control}
        label={t.forms.pesel}
        type="text"
        regex={/^\d{11}$/}
        errorMsg={t.formsErrors.pesel}
      />
      <FormInputText
        name="login"
        control={control}
        label={t.forms.login}
        type="text"
        errorMsg={t.formsErrors.login}
      />
      <FormInputText
        name="haslo"
        control={control}
        type="text"
        label={t.forms.password}
        regex={/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/}
        errorMsg={t.formsErrors.password}
      />

      <LoadingButton
        loading={isLoading}
        onClick={handleSubmit(onSubmit)}
        variant={'contained'}
      >
        {t.buttons.submit}
      </LoadingButton>
      <Button onClick={() => reset()} variant={'outlined'}>
        {t.buttons.reset}
      </Button>
    </Paper>
  )
}

export default NewDriver
