import {
  Button as BaseButton,
  TextField as BaseTextField,
  styled,
} from '@mui/material'

export const Container = styled('div')({
  width: '70%',
  display: 'flex',
  margin: '5% 15%',
  flexWrap: 'wrap',
  flexDirection: 'row',
})

export const Button = styled(BaseButton)({
  position: 'absolute',
  bottom: '10%',
  height: '60px',
  width: '120px',
  right: '17%',
  backgroundColor: '#C4C4C4',
  fontSize: '100%',
  color: '#000000',
  '&:hover': {
    backgroundColor: '#F5EAEA',
  },
})

export const Form = styled('form')({})

export const TextField = styled(BaseTextField)({
  margin: '1% 2%',
  width: '360px',
  borderRadius: '5px',
  backgroundColor: '#C4C4C4',
})
