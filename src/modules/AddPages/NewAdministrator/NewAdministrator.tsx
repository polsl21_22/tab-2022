import { useEffect } from 'react'
import { useForm } from 'react-hook-form'

import { LoadingButton } from '@mui/lab'
import { Button, Paper, Typography } from '@mui/material'
import Swal from 'sweetalert2'

import {
  useAddAdministratorMutation,
  useGetAdministratorPositionQuery,
} from 'modules/services/users.api'

import { administratorPayload, administratorPosition } from 'shared'

import { t } from 'config'

import { FormInputDropdown } from '../FormComponents/FormInputDropdown'
import { FormInputText } from '../FormComponents/FormInputText'

const defaultValues = {
  haslo: '',
  imie: '',
  login: '',
  nazwisko: '',
  pesel: '',
  stanowisko_administracyjne_id: -1,
}

const NewAdministrator = () => {
  const methods = useForm<administratorPayload>({
    defaultValues: defaultValues,
  })
  const { handleSubmit, reset, control } = methods
  const [addAdministrator, { isLoading, isError, isSuccess }] =
    useAddAdministratorMutation()
  const positionGet = useGetAdministratorPositionQuery('')
  const onSubmit = (data: administratorPayload) => {
    addAdministrator(data)
  }
  useEffect(() => {
    if (isError) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!',
      })
    }
    if (isSuccess) {
      Swal.fire('Good job!', 'Added record!', 'success')
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLoading])
  return (
    <>
      <Paper
        style={{
          display: 'grid',
          gridRowGap: '20px',
          padding: '20px',
          margin: '10px 300px',
        }}
      >
        <Typography variant="h4"> {t.pageLabels.newAdministrator}</Typography>
        {!positionGet.isLoading && (
          <FormInputDropdown
            control={control}
            options={
              positionGet.data
                ? positionGet.data.map((e: administratorPosition) => ({
                    name: e.typ,
                    id: e.id,
                  }))
                : [{ name: 'No Data', id: -1 }]
            }
            name="stanowisko_administracyjne_id"
            label={t.forms.adminPosition}
          />
        )}
        <FormInputText
          name="imie"
          control={control}
          label={t.forms.name}
          type="text"
          errorMsg={t.formsErrors.firstName}
        />
        <FormInputText
          name="nazwisko"
          control={control}
          label={t.forms.lastName}
          type="text"
          errorMsg={t.formsErrors.lastName}
        />
        <FormInputText
          name="pesel"
          control={control}
          label={t.forms.pesel}
          type="text"
          regex={/^\d{11}$/}
          errorMsg={t.formsErrors.pesel}
        />
        <FormInputText
          name="login"
          control={control}
          label={t.forms.login}
          type="text"
          errorMsg={t.formsErrors.login}
        />
        <FormInputText
          name="haslo"
          control={control}
          type="text"
          label={t.forms.password}
          regex={
            /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/
          }
          errorMsg={t.formsErrors.password}
        />

        <LoadingButton
          loading={isLoading}
          onClick={handleSubmit(onSubmit)}
          variant={'contained'}
        >
          {t.buttons.submit}
        </LoadingButton>
        <Button onClick={() => reset()} variant={'outlined'}>
          {t.buttons.reset}
        </Button>
      </Paper>
    </>
  )
}

export default NewAdministrator
