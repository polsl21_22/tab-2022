import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

import { ErrorReport, LS } from 'shared'

import { host } from 'config'

export const reportApi = createApi({
  reducerPath: 'errors',
  baseQuery: fetchBaseQuery({
    baseUrl: `${host}/backend`,
    prepareHeaders: headers => {
      headers.set('Session-id', `${LS.getSessionId()}`)
      return headers
    },
  }),
  tagTypes: ['report'],
  endpoints: builder => ({
    getReport: builder.query({
      query: () => ({
        url: '/bledy',
        method: 'GET',
      }),
      providesTags: ['report'],
    }),
    addReport: builder.mutation({
      query: (error: ErrorReport) => ({
        url: '/bledy',
        method: 'POST',
        contentType: 'application/json',
        body: error,
      }),
      invalidatesTags: ['report'],
    }),
    getReportById: builder.query({
      query: (id: number) => ({
        url: `/bledy/${id}`,
        method: 'GET',
      }),
      providesTags: ['report'],
    }),
    deleteReportById: builder.mutation({
      query: (id: number) => ({
        url: `/bledy/${id}`,
        method: 'DELETE',
      }),
      invalidatesTags: ['report'],
    }),
  }),
})
export const {
  useAddReportMutation,
  useGetReportByIdQuery,
  useGetReportQuery,
  useDeleteReportByIdMutation,
} = reportApi
