import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import moment from 'moment'

import {
  LS,
  administratorPayload,
  administratorPositionPayload,
  authPayLoad,
  driverPayload,
  manufacturePayload,
  passingPayload,
  patchRoutePayload,
  routePayload,
  vehiclePayload,
} from 'shared'

import { host } from 'config'

export const authApi = createApi({
  reducerPath: 'users',
  baseQuery: fetchBaseQuery({
    baseUrl: `${host}`,
    credentials: 'include',
  }),
  endpoints: builder => ({
    auth: builder.mutation({
      query: (auth: authPayLoad) => ({
        url: `/auth/${auth.level}`,
        method: 'POST',
        contentType: 'application/json',
        body: auth.credentials,
      }),
      transformResponse: (data: { session_id: string }) => {
        const success = data && data.session_id ? true : false
        success ? LS.setSessionId(data.session_id) : LS.Logout()
      },
    }),
  }),
})

export const userApi = createApi({
  reducerPath: 'users',
  baseQuery: fetchBaseQuery({
    baseUrl: `${host}/backend`,
    prepareHeaders: headers => {
      headers.set('Session-id', `${LS.getSessionId()}`)
      return headers
    },
  }),
  endpoints: builder => ({
    getDrivers: builder.query({
      query: () => ({
        url: '/kierowcy',
        method: 'GET',
      }),
    }),
    addDriver: builder.mutation({
      query: (driver: driverPayload) => ({
        url: '/kierowcy',
        method: 'POST',
        contentType: 'application/json',
        body: driver,
      }),
    }),
    getRoutes: builder.query({
      query: () => ({
        url: `/kursy`,
        method: 'GET',
      }),
    }),
    getRoutesById: builder.query({
      query: (id: number) => ({
        url: `/kursy/kierowca/${id}`,
        method: 'GET',
      }),
    }),
    getFilteredRoutes: builder.query({
      query: (queryString: string) => ({
        url: `/kursy${queryString}`,
        method: 'GET',
      }),
    }),
    getMyRoutes: builder.query({
      query: (queryString: string) => ({
        url: `/kursy/me?data_zakonczenia_${
          queryString + '=' + moment().toISOString()
        }`,
        method: 'GET',
      }),
    }),
    addRoute: builder.mutation({
      query: (route: routePayload) => ({
        url: '/kursy',
        method: 'POST',
        contentType: 'application/json',
        body: route,
      }),
    }),
    getPassings: builder.query({
      query: () => ({
        url: '/trasy',
        method: 'GET',
      }),
    }),
    addPassing: builder.mutation({
      query: (passing: passingPayload) => ({
        url: '/trasy',
        method: 'POST',
        contentType: 'application/json',
        body: passing,
      }),
    }),
    getVehicles: builder.query({
      query: () => ({
        url: '/pojazdy',
        method: 'GET',
      }),
    }),
    addVehicle: builder.mutation({
      query: (vehicle: vehiclePayload) => ({
        url: '/pojazdy',
        method: 'POST',
        contentType: 'application/json',
        body: vehicle,
      }),
    }),
    getManufacture: builder.query({
      query: () => ({
        url: '/marki',
        method: 'GET',
      }),
    }),
    getAdministrator: builder.query({
      query: () => ({
        url: '/administracja',
        method: 'GET',
      }),
    }),
    addManufacture: builder.mutation({
      query: (manufacture: manufacturePayload) => ({
        url: '/marki',
        method: 'POST',
        contentType: 'application/json',
        body: manufacture,
      }),
    }),
    getAdministratorPosition: builder.query({
      query: () => ({
        url: '/stanowiska_administracyjne',
        method: 'GET',
      }),
    }),
    addAdministratorPosition: builder.mutation({
      query: (position: administratorPositionPayload) => ({
        url: '/stanowiska_administracyjne',
        method: 'POST',
        contentType: 'application/json',
        body: position,
      }),
    }),
    addAdministrator: builder.mutation({
      query: (administrator: administratorPayload) => ({
        url: '/administracja',
        method: 'POST',
        contentType: 'application/json',
        body: administrator,
      }),
    }),
    getCategories: builder.query({
      query: () => ({
        url: '/kategoria_prawa_jazdy',
        method: 'GET',
      }),
    }),
    addPatchRoute: builder.mutation({
      query: (payload: { id: number; patchRoute: patchRoutePayload }) => ({
        url: `/kursy/${payload.id}`,
        method: 'PATCH',
        contentType: 'application/json',
        body: payload.patchRoute,
      }),
    }),
  }),
})
export const {
  useGetDriversQuery,
  useAddDriverMutation,
  useGetFilteredRoutesQuery,
  useGetRoutesByIdQuery,
  useGetRoutesQuery,
  useGetMyRoutesQuery,
  useGetPassingsQuery,
  useAddPassingMutation,
  useAddManufactureMutation,
  useGetVehiclesQuery,
  useGetAdministratorQuery,
  useAddVehicleMutation,
  useAddRouteMutation,
  useGetManufactureQuery,
  useGetAdministratorPositionQuery,
  useAddAdministratorPositionMutation,
  useAddAdministratorMutation,
  useGetCategoriesQuery,
  useAddPatchRouteMutation,
} = userApi

export const { useAuthMutation } = authApi
