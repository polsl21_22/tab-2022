import { FC } from 'react'

import { BackButton, NavBar, TemplatePropType } from 'shared'

const Template: FC<TemplatePropType> = ({ component: Component }) => {
  return (
    <>
      <NavBar />
      <Component />
      <BackButton />
    </>
  )
}

export default Template
