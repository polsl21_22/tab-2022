import { useEffect } from 'react'
import { useForm } from 'react-hook-form'
import { useLocation } from 'react-router-dom'

import { LoadingButton } from '@mui/lab'
import { Button, Paper, Typography } from '@mui/material'
import Swal from 'sweetalert2'

import { FormInputDate } from 'modules/AddPages/FormComponents/FormInputDate'
import { FormInputText } from 'modules/AddPages/FormComponents/FormInputText'
import { useAddPatchRouteMutation } from 'modules/services/users.api'

import { patchRoutePayload } from 'shared'
import { route } from 'shared'

import { t } from 'config'

const PatchRoute = () => {
  const location = useLocation()
  const state = location.state as route
  const defaultValues = {
    czas_rozpoczecia: state.data_rozpoczecia,
    czas_zakonczenia: state.data_zakonczenia,
    czas_przejazdu: 0,
  }
  const methods = useForm<patchRoutePayload>({
    defaultValues: defaultValues,
  })
  const [addPatchRoute, { isError, isLoading, isSuccess }] =
    useAddPatchRouteMutation()
  const { handleSubmit, reset, control } = methods

  const onSubmit = (data: patchRoutePayload) => {
    data.czas_przejazdu = +data.czas_przejazdu
    addPatchRoute({ id: state.id, patchRoute: data })
  }
  useEffect(() => {
    if (isError) {
      Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!',
      })
    }
    if (isSuccess) {
      Swal.fire('Good job!', 'Updated record!', 'success')
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isLoading])
  return (
    <>
      <Paper
        style={{
          display: 'grid',
          gridRowGap: '20px',
          padding: '20px',
          margin: '10px 300px',
        }}
      >
        <Typography variant="h4"> {t.pageLabels.newRoute}</Typography>
        <FormInputDate
          name="czas_rozpoczecia"
          control={control}
          label={t.forms.startDate}
        />
        <FormInputDate
          name="czas_zakonczenia"
          control={control}
          label={t.forms.endDate}
        />
        <FormInputText
          name="czas_przejazdu"
          control={control}
          label={t.forms.passingTime}
          type="number"
          errorMsg={t.formsErrors.load}
          regex={
            /^(([0-5]?\d{0,4}|6[0-4]\d{3}|65[0-4]\d{2}|655[0-2]\d|6553[0-5])(,|(?=$)))+$/
          }
        />
        <LoadingButton
          loading={isLoading}
          onClick={handleSubmit(onSubmit)}
          variant={'contained'}
        >
          {t.buttons.submit}
        </LoadingButton>
        <Button onClick={() => reset()} variant={'outlined'}>
          {t.buttons.reset}
        </Button>
      </Paper>
    </>
  )
}
export default PatchRoute
