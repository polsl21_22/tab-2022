import LoginForm from 'modules/Auth/components'

import { Logo } from 'shared'

import { Container, FormContainer } from './LoginPage.style'

const LoginPage = () => {
  return (
    <Container>
      <Logo />
      <FormContainer>
        <LoginForm />
      </FormContainer>
    </Container>
  )
}

export default LoginPage
