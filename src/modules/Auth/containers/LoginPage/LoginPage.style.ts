import { styled } from '@mui/material'

export const FormContainer = styled('div')({
  height: '100%',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
})

export const Container = styled('div')({
  height: '95vh',
})
