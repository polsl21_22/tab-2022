import {
  Button as BaseButton,
  TextField as BaseTextField,
  ToggleButton as BaseToggleButton,
  ToggleButtonGroup as BaseToggleButtonGroup,
  styled,
} from '@mui/material'

export const Div = styled('div')({
  backgroundColor: 'rgba(256, 256, 256, 0.05)',
  borderRadius: '40px',
  color: '#FFFFFF',
  height: '600px',
  width: '500px',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'center',
})

export const Form = styled('form')({
  marginTop: '50px',
  width: '60%',
})

export const TextField = styled(BaseTextField)({
  borderRadius: '5px',
  backgroundColor: '#C4C4C4',
})

export const ToggleButtonGroup = styled(BaseToggleButtonGroup)({
  backgroundColor: 'rgba(192,192,192,0.4)',
})

export const Button = styled(BaseButton)({
  marginTop: '20px',
  height: '60px',
  outline: 'none',
})

export const ToggleButton = styled(BaseToggleButton)({
  '&.Mui-selected, &.Mui-selected:hover': {
    backgroundColor: 'rgba(192,192,192,0.5)',
  },
  width: '95px',
})

export const ErrorContainer = styled('p')({
  color: 'red',
  height: '20px',
  textAlign: 'center',
})
