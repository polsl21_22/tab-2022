import { useState } from 'react'
import { useNavigate } from 'react-router-dom'

import { Visibility, VisibilityOff } from '@mui/icons-material'
import { IconButton, InputAdornment } from '@mui/material'
import { ChangeBackground } from 'index'

import { useAuthMutation } from 'modules'

import {
  AdminUserIcon,
  DatabaseUserIcon,
  DriverUserIcon,
  LS,
  PickBackground,
  userType,
} from 'shared'

import { paths, t } from 'config'

import {
  Button,
  Div,
  ErrorContainer,
  Form,
  TextField,
  ToggleButton,
  ToggleButtonGroup,
} from './LoginForm.style'

const LoginForm = () => {
  const [state, setState] = useState<userType>(userType.Admin)
  const [error, setError] = useState('')

  const [login, setLogin] = useState<string>('')
  const [password, setPassword] = useState<string>('')
  const [showPassword, setShowPassword] = useState(false)
  const handleClickShowPassword = () => setShowPassword(!showPassword)
  const handleMouseDownPassword = () => setShowPassword(!showPassword)

  const [auth] = useAuthMutation()

  const navigate = useNavigate()

  const handleChange = (event: any, newState: userType) => {
    if (newState) {
      setState(newState)
      ChangeBackground(PickBackground(newState))
    }
  }

  const handleSubmit = async () => {
    LS.setUserType(state)
    await auth({
      level: parseInt(state),
      credentials: {
        login: login,
        haslo: password,
      },
    })
    LS.isLoggedin() ? navigate(paths.dashboard) : setError(t.errors.loginError)
  }

  return (
    <Div>
      <ToggleButtonGroup
        color="primary"
        value={state}
        exclusive
        onChange={handleChange}
      >
        <ToggleButton value={userType.Admin}>
          <AdminUserIcon />
        </ToggleButton>
        <ToggleButton value={userType.Driver}>
          <DriverUserIcon />
        </ToggleButton>
        <ToggleButton value={userType.Database}>
          <DatabaseUserIcon />
        </ToggleButton>
      </ToggleButtonGroup>
      <Form onSubmit={handleSubmit}>
        <p>{t.forms.login}</p>
        <TextField
          id="login"
          variant="outlined"
          fullWidth
          value={login}
          onChange={event => setLogin(event.target.value)}
        />
        <p>{t.forms.password}</p>
        <TextField
          id="password"
          fullWidth
          variant="outlined"
          value={password}
          onChange={event => setPassword(event.target.value)}
          type={showPassword ? 'text' : 'password'}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton
                  aria-label="toggle password visibility"
                  onClick={handleClickShowPassword}
                  onMouseDown={handleMouseDownPassword}
                >
                  {showPassword ? <Visibility /> : <VisibilityOff />}
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
        <ErrorContainer>{error}</ErrorContainer>
        <Button
          variant="contained"
          color="primary"
          fullWidth
          onClick={handleSubmit}
        >
          {t.buttons.login}
        </Button>
      </Form>
    </Div>
  )
}

export default LoginForm
