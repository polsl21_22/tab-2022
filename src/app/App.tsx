import { Provider } from 'react-redux'

import { combineReducers, configureStore } from '@reduxjs/toolkit'

import { HistoryRouter, SharedRoutes, authMiddleware, history } from 'modules'
import { reportApi, userApi } from 'modules'

const rootReducer = combineReducers({
  [userApi.reducerPath]: userApi.reducer,
  [reportApi.reducerPath]: reportApi.reducer,
})
export const store = configureStore({
  reducer: rootReducer,
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware()
      .concat(userApi.middleware)
      .concat(reportApi.middleware)
      .concat(authMiddleware),
})

const App = () => {
  return (
    <Provider store={store}>
      <HistoryRouter history={history}>
        <SharedRoutes />
      </HistoryRouter>
    </Provider>
  )
}

export default App
