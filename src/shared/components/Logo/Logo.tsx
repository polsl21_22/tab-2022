import { t } from 'config'

import { LogoContainer } from './Logo.style'

const Logo = () => {
  return (
    <LogoContainer>
      <p>{t.title}</p>
    </LogoContainer>
  )
}

export default Logo
