import { styled } from '@mui/material'

export const LogoContainer = styled('div')({
  height: '5%',
  margin: '-2% 5%',
  color: '#FFFFFF',
  fontSize: '50px',
})
