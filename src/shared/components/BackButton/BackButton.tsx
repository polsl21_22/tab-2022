import { useNavigate } from 'react-router-dom'

import { Button } from './BackButton.style'

const BackButton = () => {
  const navigate = useNavigate()

  const handleClick = () => {
    navigate(-1)
  }

  return (
    <Button variant="contained" color="primary" onClick={handleClick}>
      back
    </Button>
  )
}

export default BackButton
