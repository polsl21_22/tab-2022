import { Button as BaseButton, styled } from '@mui/material'

export const Button = styled(BaseButton)({
  height: '60px',
  width: '120px',
  marginLeft: '10%',
  backgroundColor: '#C4C4C4',
  fontSize: '100%',
  color: '#000000',
  '&:hover': {
    backgroundColor: '#F5EAEA',
  },
})
