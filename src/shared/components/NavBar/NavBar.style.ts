import { Button as BaseButton, styled } from '@mui/material'

export const Container = styled('div')({
  display: 'flex',
  height: '100%',
  justifyContent: 'space-between',
  flexDirection: 'row',
  alignItems: 'center',
})

export const Button = styled(BaseButton)({
  height: '40%',
  padding: '25px',
  backgroundColor: '#C4C4C4',
  color: '#000000',
})

export const ButtonContainer = styled('div')({
  display: 'grid',
  placeItems: 'center',
  marginRight: '5%',
})
