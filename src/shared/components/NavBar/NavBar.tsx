import { useNavigate } from 'react-router-dom'

import { ChangeBackground } from 'index'

import { LS, userType } from 'shared'
import { PickBackground } from 'shared/services'

import { paths, t } from 'config'

import Logo from '../Logo'
import { Button, ButtonContainer, Container } from './NavBar.style'

const NavBar = () => {
  const navigate = useNavigate()

  const handleLogout = () => {
    ChangeBackground(PickBackground(userType.Admin))
    LS.Logout()
    navigate(paths.login)
  }

  return (
    <Container>
      <Logo />
      <ButtonContainer>
        <Button variant="contained" color="primary" onClick={handleLogout}>
          {t.buttons.logout}
        </Button>
      </ButtonContainer>
    </Container>
  )
}

export default NavBar
