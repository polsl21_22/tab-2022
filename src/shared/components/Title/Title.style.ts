import { styled } from '@mui/material'

export const TitleContainer = styled('p')({
  height: '5%',
  margin: '0 5% 0 10%',
  color: '#FFFFFF',
  fontSize: '30px',
})
