import { TitleContainer } from './Title.style'

const Title = ({ title }: { title: string }) => {
  return <TitleContainer>{title}</TitleContainer>
}
export default Title
