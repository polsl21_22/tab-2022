import DownloadIcon from '@mui/icons-material/Download'
import { Button } from '@mui/material'
import {
  GridToolbarColumnsButton,
  GridToolbarContainer,
  GridToolbarDensitySelector,
} from '@mui/x-data-grid'

import { downloadExcel, downloadPDF, tableType } from 'shared'

const CustomToolBar = (
  title: string,
  data: any,
  type: tableType,
  additionalTitle: string
) => {
  return (
    <GridToolbarContainer>
      <GridToolbarColumnsButton />
      <GridToolbarDensitySelector />
      <Button
        onClick={() => {
          downloadPDF(title, data, type, additionalTitle)
        }}
      >
        <DownloadIcon />
        Download PDF
      </Button>
      <Button
        onClick={() => {
          downloadExcel(title, data, type)
        }}
      >
        <DownloadIcon />
        Download Excel
      </Button>
    </GridToolbarContainer>
  )
}
export default CustomToolBar
