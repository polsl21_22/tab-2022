export enum tableType {
  monthlyTable,
  filteredTable,
  driversTable,
  driverTable,
  adminsTable,
}

export interface TemplatePropType {
  component: React.FC
}

export interface DashBoardButtonProps {
  path: string
  label: string
}
