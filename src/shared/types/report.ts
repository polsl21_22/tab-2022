export interface ErrorReport {
  id: number,
  tytul: string,
  kategoria: string,
  opis: string,
  data_utworzenia: Date,
}