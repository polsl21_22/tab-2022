export interface authPayLoad {
  level: number
  credentials: {
    login: string
    haslo: string
  }
}

export interface categories {
  id: number
  kategoria: string
}

export interface driver {
  id: number
  imie: string
  kategorie: categories[]
  kierowca_id: number
  login: string
  nazwisko: string
  pesel: string
}

export interface administrator {
  id: number
  imie: string
  StanowiskoAdministracyjne: position
  administracja_id: number
  login: string
  nazwisko: string
  pesel: string
  stanowisko_administracyjne_id: number
}

export interface position {
  id: number
  typ: string
}

export interface manufacture {
  id: number
  nazwa: string
}

export interface passing {
  id: number
  miejscowosc_poczatkowa: string
  miejscowosc_koncowa: string
}

export interface vehicle {
  Marka: manufacture
  id: number
  marka_id: number
  model: string
  numer_rejestracyjny: string
  pojemnosc_silnika: number
  vin: string
}

export interface route {
  id: number
  data_rozpoczecia: Date
  data_zakonczenia: Date
  czas_przejazdu: number
  ladunek: number
  trasa_id: number
  kierowca_id: number
  pojazd_id: number
  Kierowca: driver
  Pojazd: vehicle
  Trasa: passing
}
export interface administratorPosition {
  typ: string
  id: number
}
