export interface driverPayload {
  pesel: string
  imie: string
  nazwisko: string
  kategorie: number[]
  login: string
  haslo: string
}

export interface passingPayload {
  miejscowosc_poczatkowa: string
  miejscowosc_koncowa: string
}

export interface vehiclePayload {
  vin: string
  pojemnosc_silnika: number
  marka_id: number
  numer_rejestracyjny: string
  model: string
}

export interface manufacturePayload {
  nazwa: string
  id: number
}

export interface administratorPositionPayload {
  typ: string
}

export interface administratorPayload {
  haslo: string
  imie: string
  login: string
  nazwisko: string
  pesel: string
  stanowisko_administracyjne_id: number
}

export interface routePayload {
  data_rozpoczecia: Date
  data_zakonczenia: Date
  czas_przejazdu: number
  ladunek: number
  trasa_id: number
  kierowca_id: number
  pojazd_id: number
}

export interface patchRoutePayload {
  czas_rozpoczecia: Date
  czas_zakonczenia: Date
  czas_przejazdu: number
}
