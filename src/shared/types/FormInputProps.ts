export interface FormInputProps {
  name: string
  control: any
  label: string
  setValue?: any
  regex?: any
  errorMsg?: string
  options?: any
  type?: string
  getValues?: any
}
