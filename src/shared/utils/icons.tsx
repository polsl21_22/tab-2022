export const AdminUserIcon = () => {
  return (
    <svg
      width="40"
      height="60"
      viewBox="0 0 40 60"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <ellipse cx="20" cy="41.25" rx="20" ry="18.75" fill="#C4C4C4" />
      <rect y="41.25" width="40" height="18.75" fill="#C4C4C4" />
      <ellipse cx="20" cy="11.25" rx="12" ry="11.25" fill="#C4C4C4" />
    </svg>
  )
}
export const DriverUserIcon = () => {
  return (
    <svg
      width="50"
      height="60"
      viewBox="0 0 50 60"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <rect y="20" width="50" height="28" fill="#C4C4C4" />
      <ellipse cx="6.66667" cy="52" rx="6.66667" ry="8" fill="#C4C4C4" />
      <ellipse cx="42.3334" cy="52" rx="6.66667" ry="8" fill="#C4C4C4" />
      <rect x="23.3333" width="26.6667" height="20" fill="#C4C4C4" />
    </svg>
  )
}
export const DatabaseUserIcon = () => {
  return (
    <svg
      width="50"
      height="60"
      viewBox="0 0 50 60"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <ellipse cx="25" cy="51.8919" rx="25" ry="8.10811" fill="#C4C4C4" />
      <rect y="43.7838" width="50" height="8.10811" fill="#C4C4C4" />
      <ellipse cx="25" cy="42.1622" rx="25" ry="8.10811" fill="#B6B6B6" />
      <ellipse cx="25" cy="35.6757" rx="25" ry="8.10811" fill="#C4C4C4" />
      <rect y="27.5676" width="50" height="8.10811" fill="#C4C4C4" />
      <ellipse cx="25" cy="25.9459" rx="25" ry="8.10811" fill="#B6B6B6" />
      <ellipse cx="25" cy="17.8378" rx="25" ry="8.10811" fill="#C4C4C4" />
      <rect y="9.72973" width="50" height="8.10811" fill="#C4C4C4" />
      <ellipse cx="25" cy="8.10811" rx="25" ry="8.10811" fill="#B6B6B6" />
    </svg>
  )
}
