import { faker } from '@faker-js/faker'
import moment from 'moment'

faker.setLocale('pl')
moment.locale('pl')

const dataCount = faker.datatype.number({ min: 200, max: 400, precision: 1 })

const createData = (
  id: number,
  driver_name: string,
  driver_last_name: string,
  start_time: string,
  end_time: string,
  vehicle: string,
  route: string,
  total_time: number | null
) => {
  return {
    id,
    driver_name,
    driver_last_name,
    start_time,
    end_time,
    vehicle,
    route,
    total_time,
  }
}

export const fakeDataObject = (i: number, name: string, last_name: string) => {
  const start = faker.date.past()
  const end = moment(start).add(
    faker.datatype.number({ min: 2, max: 15, precision: 1 }),
    'days'
  )
  return createData(
    i,
    name,
    last_name,
    moment(start).format('DD/MM/YYYY'),
    moment(end).format('DD/MM/YYYY'),
    faker.vehicle.manufacturer() +
      ' ' +
      faker.vehicle.model() +
      ' #' +
      faker.datatype.number({ min: 1, max: 20, precision: 1 }),
    faker.address.cityName() + ' - ' + faker.address.cityName(),
    parseInt(faker.random.numeric(2))
  )
}

const fakeData = () => {
  let sampleData = []
  let name = faker.name.firstName()
  let last_name = faker.name.lastName()
  let random = faker.datatype.number({ min: 25, max: 40, precision: 1 })
  for (let i = 2; i < dataCount - 1; i++) {
    if (i % random) {
      name = faker.name.firstName()
      last_name = faker.name.lastName()
    }
    sampleData.push(fakeDataObject(i, name, last_name))
  }
  return sampleData
}

export const fakeDriver = () => {
  const name = faker.name.firstName()
  return {
    pesel: `${faker.random.numeric(11)}`,
    imie: name,
    nazwisko: faker.name.lastName(),
    login: faker.internet.userName(name),
    haslo: faker.internet.password(),
  }
}
export const fakePassing = () => {
  return {
    miejscowosc_poczatkowa: faker.address.cityName(),
    miejscowosc_koncowa: faker.address.cityName(),
  }
}

export const fakeManufacture = () => {
  return {
    nazwa: faker.vehicle.manufacturer(),
  }
}

export const fakeVehicle = () => {
  return {
    vin: faker.vehicle.vin(),
    pojemnosc_silnika: parseInt(faker.random.numeric(4)),
    marka_id: faker.datatype.number({ min: 1, max: 21, precision: 1 }),
    model: faker.vehicle.model(),
    numer_rejestracyjny: faker.vehicle.vrm(),
  }
}

export const fakeRoute = () => {
  const start = faker.date.past()
  const end = moment(start)
    .add(faker.datatype.number({ min: 2, max: 15, precision: 1 }), 'days')
    .toDate()
  return {
    data_rozpoczecia: start,
    data_zakonczenia: end,
    czas_rozpoczecia: moment(start)
      .add(faker.datatype.number({ min: -1, max: 1, precision: 1 }), 'days')
      .toDate(),
    czas_zakonczenia: moment(end)
      .add(faker.datatype.number({ min: -1, max: 1, precision: 1 }), 'days')
      .toDate(),
    czas_przejazdu: parseInt(faker.random.numeric(2)),
    ladunek: faker.datatype.number({ min: 500, max: 8900, precision: 1 }),
    trasa_id: faker.datatype.number({ min: 2, max: 85, precision: 1 }),
    kierowca_id: faker.datatype.number({ min: 2, max: 132, precision: 1 }),
    pojazd_id: faker.datatype.number({ min: 2, max: 58, precision: 1 }),
  }
}

export let data = fakeData()
