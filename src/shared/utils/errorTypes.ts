export const errorTypes = [
  {
    id: 0,
    nazwa: 'Bledne dane',
  },
  {
    id: 1,
    nazwa: 'Problem z pojazdem',
  },
  {
    id: 2,
    nazwa: 'Usuniecie pracownika',
  },
  {
    id: 3,
    nazwa: 'Problem ze strona',
  },
  {
    id: 4,
    nazwa: 'Niesklasyfikowany blad',
  },
]