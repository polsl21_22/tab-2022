import { paths, t } from 'config'

export const adminRoutes = [
  {
    path: paths.addPassing,
    label: t.pageLabels.newPassing,
  },
  {
    path: paths.addRoute,
    label: t.pageLabels.newRoute,
  },
  {
    path: paths.addVehicle,
    label: t.pageLabels.newVehicle,
  },
  {
    path: paths.monthlyPlan,
    label: t.pageLabels.monthlyTable,
  },
  {
    path: paths.driverPlan,
    label: t.pageLabels.driverTable,
  },
  {
    path: paths.allRecords,
    label: t.pageLabels.filteredTable,
  },
  {
    path: paths.allDrivers,
    label: t.pageLabels.driversTable,
  },
  {
    path: paths.addDriver,
    label: t.pageLabels.newDriver,
  },
  {
    path: paths.reportBug,
    label: t.pageLabels.reportBug,
  },
]

export const driverRoutes = [
  {
    path: paths.driverSinglePlan,
    label: t.pageLabels.myPlan,
  },
  {
    path: paths.myRecords,
    label: t.pageLabels.myRecords,
  },
  {
    path: paths.reportBug,
    label: t.pageLabels.reportBug,
  },
]

export const databaseRoutes = [
  {
    path: paths.addPassing,
    label: t.pageLabels.newPassing,
  },
  {
    path: paths.addRoute,
    label: t.pageLabels.newRoute,
  },
  {
    path: paths.addVehicle,
    label: t.pageLabels.newVehicle,
  },
  {
    path: paths.addDriver,
    label: t.pageLabels.newDriver,
  },
  {
    path: paths.addAdministrator,
    label: t.pageLabels.newAdministrator,
  },
  {
    path: paths.monthlyPlan,
    label: t.pageLabels.monthlyTable,
  },
  {
    path: paths.driverPlan,
    label: t.pageLabels.driverTable,
  },
  {
    path: paths.allRecords,
    label: t.pageLabels.filteredTable,
  },
  {
    path: paths.allDrivers,
    label: t.pageLabels.driversTable,
  },
  {
    path: paths.allAdmins,
    label: t.pageLabels.administratorsTable,
  },
  {
    path: paths.readBugs,
    label: t.pageLabels.readBugs,
  },
  {
    path: paths.pgAdmin,
    label: t.pageLabels.pgAdmin,
  },
]
