import jsPDF from 'jspdf'
import autoTable from 'jspdf-autotable'
import moment from 'moment'
import * as XLSX from 'xlsx'

import { administrator, categories, driver, route, tableType } from 'shared'

import { callAddFont } from './Amiko-Regular-normal'

export const categoriesString = (categories: categories[]) => {
  var newString = ''
  if (categories) {
    categories.forEach(cat => (newString = newString + cat.kategoria + ', '))
  }
  if (newString.length > 3) newString = newString.slice(0, -2)
  return newString
}

export const formatDataRoutes = (data: route[]) => {
  const newData = data.map(row => {
    return {
      id: row.id,
      data:
        moment(row.data_rozpoczecia).format('DD.MM.YYYY') +
        ' - ' +
        moment(row.data_zakonczenia).format('DD.MM.YYYY'),
      trasa:
        row.Trasa.miejscowosc_poczatkowa +
        ' - ' +
        row.Trasa.miejscowosc_koncowa,
      czas_przejazdu: row.czas_przejazdu,
      ladunek: row.ladunek,
      kierowca:
        row.Kierowca.imie +
        ' ' +
        row.Kierowca.nazwisko +
        ' (' +
        row.Kierowca.login +
        ')',
      pojazd:
        row.Pojazd.Marka.nazwa +
        ' ' +
        row.Pojazd.model +
        ' ( ' +
        row.Pojazd.numer_rejestracyjny +
        ' )',
    }
  })
  return newData
}

export const formatDataDrivers = (data: driver[]) => {
  const newData = data.map(row => {
    return {
      id: row.id,
      imie: row.imie,
      nazwisko: row.nazwisko,
      login: row.login,
      PESEL: row.pesel,
      kategorie_pj: categoriesString(row.kategorie),
    }
  })
  return newData
}

export const formatDataAdmins = (data: administrator[]) => {
  const newData = data.map(row => {
    return {
      id: row.id,
      imie: row.imie,
      nazwisko: row.nazwisko,
      login: row.login,
      PESEL: row.pesel,
      stanowisko: row.StanowiskoAdministracyjne.typ,
    }
  })
  return newData
}

export const formatDataDriver = (data: route[]) => {
  const newData = data.map(row => {
    return {
      id: row.id,
      data:
        moment(row.data_rozpoczecia).format('DD.MM.YYYY') +
        ' - ' +
        moment(row.data_zakonczenia).format('DD.MM.YYYY'),
      trasa:
        row.Trasa.miejscowosc_poczatkowa +
        ' - ' +
        row.Trasa.miejscowosc_koncowa,
      czas_przejazdu: row.czas_przejazdu,
      ladunek: row.ladunek,
      pojazd:
        row.Pojazd.Marka.nazwa +
        ' ' +
        row.Pojazd.model +
        ' (' +
        row.Pojazd.numer_rejestracyjny +
        ')',
    }
  })
  return newData
}

const columnsDrivers = [
  { header: 'Id', dataKey: 'id' },
  { header: 'Name', dataKey: 'imie' },
  { header: 'Last Name', dataKey: 'nazwisko' },
  { header: 'login', dataKey: 'login' },
  { header: 'PESEL', dataKey: 'PESEL' },
  { header: 'DL categories', dataKey: 'kategorie_pj' },
]

const columnsDriver = [
  { header: 'Id', dataKey: 'id' },
  { header: 'Date', dataKey: 'data' },
  { header: 'Road', dataKey: 'trasa' },
  { header: 'Time', dataKey: 'czas_przejazdu' },
  { header: 'Load (kg)', dataKey: 'ladunek' },
  { header: 'Vehicle', dataKey: 'pojazd' },
]

const columnsRoutes = [
  { header: 'Id', dataKey: 'id' },
  { header: 'Date', dataKey: 'data' },
  { header: 'Road', dataKey: 'trasa' },
  { header: 'Time', dataKey: 'czas_przejazdu' },
  { header: 'Load (kg)', dataKey: 'ladunek' },
  { header: 'Driver', dataKey: 'kierowca' },
  { header: 'Vehicle', dataKey: 'pojazd' },
]

const columnsAdmin = [
  { header: 'Id', dataKey: 'id' },
  { header: 'Name', dataKey: 'imie' },
  { header: 'Last Name', dataKey: 'nazwisko' },
  { header: 'login', dataKey: 'login' },
  { header: 'PESEL', dataKey: 'PESEL' },
  { header: 'Position', dataKey: 'stanowisko' },
]

const pickDataAndColumns = (type: tableType, data: any) => {
  var newData = pickData(type, data)
  var newColumns
  switch (type) {
    case tableType.driverTable:
      newColumns = columnsDriver
      break
    case tableType.driversTable:
      newColumns = columnsDrivers
      break
    case tableType.adminsTable:
      newColumns = columnsAdmin
      break
    case tableType.monthlyTable:
    case tableType.filteredTable:
    default:
      newColumns = columnsRoutes
  }
  return { data: newData, columns: newColumns }
}

const pickData = (type: tableType, data: any) => {
  switch (type) {
    case tableType.driverTable:
      return formatDataDriver(data as route[])
    case tableType.driversTable:
      return formatDataDrivers(data as driver[])
    case tableType.adminsTable:
      return formatDataAdmins(data as administrator[])
    case tableType.monthlyTable:
    case tableType.filteredTable:
    default:
      return formatDataRoutes(data as route[])
  }
}

export const downloadPDF = (
  title: string,
  data: any,
  type: tableType,
  additionalTitle: string
) => {
  const doc = new jsPDF()
  const pdfData = pickDataAndColumns(type, data)
  doc.setLanguage('pl')
  callAddFont(doc)

  doc.setFontSize(11)
  doc.text(additionalTitle, 15, 10)
  autoTable(doc, {
    body: pdfData.data,
    columns: pdfData.columns,
    styles: { font: 'Amiko-Regular' },
  })

  doc.save(title + '.pdf')
}

export const downloadExcel = (title: string, data: any, type: tableType) => {
  const workSheet = XLSX.utils.json_to_sheet(pickData(type, data))
  const workBook = XLSX.utils.book_new()
  XLSX.utils.book_append_sheet(workBook, workSheet, 'table')
  XLSX.write(workBook, { bookType: 'xlsx', type: 'binary' })
  XLSX.writeFile(workBook, title + '.xlsx')
}
