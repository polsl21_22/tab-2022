export enum colors {
  AdminColor = '#223421',
  DriverColor = '#212F34',
  DatabaseColor = '#342121',
}
