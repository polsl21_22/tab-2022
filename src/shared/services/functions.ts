import { colors, userType } from 'shared/utils'

export const PickBackground = (user: userType) => {
  switch (user) {
    case userType.Admin: {
      return colors.AdminColor
    }
    case userType.Driver: {
      return colors.DriverColor
    }
    case userType.Database: {
      return colors.DatabaseColor
    }
  }
}
