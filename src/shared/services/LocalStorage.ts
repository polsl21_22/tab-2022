class Localstorage {
  static setUserType(account_type: string) {
    localStorage.setItem('account_type', account_type)
  }

  static getUserType() {
    return localStorage.getItem('account_type')
  }

  static setSessionId(sessionId: string) {
    localStorage.setItem('sessionId', sessionId)
  }

  static getSessionId() {
    return localStorage.getItem('sessionId')
  }

  static isLoggedin() {
    if (!!Localstorage.getSessionId() && !!Localstorage.getUserType()) {
      return true
    }
    localStorage.removeItem('sessionId')
    localStorage.removeItem('account_type')
    return false
  }

  static Logout() {
    localStorage.removeItem('sessionId')
    localStorage.removeItem('account_type')
  }
}
export default Localstorage
